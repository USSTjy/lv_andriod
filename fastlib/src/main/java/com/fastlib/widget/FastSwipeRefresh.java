package com.fastlib.widget;

import android.content.Context;
import androidx.core.view.MotionEventCompat;

import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/**
 * 偷懒版SwipeRefreshLayout.可以包裹非AbsListView正常使用
 */
public class FastSwipeRefresh extends FrameLayout{
    private RefreshLayout mRefresh;
    private StateListView mDefaultListView;

    public FastSwipeRefresh(Context context, AttributeSet attrs) {
        super(context,attrs);
        mRefresh=new RefreshLayout(context,attrs);
        mDefaultListView=new StateListView(context);
        mRefresh.addView(mDefaultListView);
        addView(mRefresh);
        setOnHierarchyChangeListener(new OnHierarchyChangeListener(){ //当视图树变动时,使refresh始终显示在最上层
            @Override
            public void onChildViewAdded(View parent, View child) {
                if(child!=mRefresh){
                    removeView(mRefresh);
                    addView(mRefresh);
                }
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {

            }
        });
    }

    public RefreshLayout getRefresh(){
        return mRefresh;
    }

    public StateListView getListView(){
        return mDefaultListView;
    }
}
