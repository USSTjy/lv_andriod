package com.fastlib.bean;

/**
 * Created by Administrator on 2017/9/19.
 */

public class RemotePermissionResult {
    private int mCode;
    private String[] mPermissions;
    private int[] mGrantPermission;

    public RemotePermissionResult(int mCode, String[] mPermissions, int[] mGrantPermission) {
        this.mCode = mCode;
        this.mPermissions = mPermissions;
        this.mGrantPermission = mGrantPermission;
    }

    public int getmCode() {
        return mCode;
    }

    public String[] getmPermissions() {
        return mPermissions;
    }

    public int[] getmGrantPermission() {
        return mGrantPermission;
    }
}
