package com.fastlib.bean;

/**
 * Created by Administrator on 2017/9/19.
 */

public class RemoteRequestPermission {
    private int mRequestCode;
    private String mPermission;

    public RemoteRequestPermission(int mRequestCode, String mPermission) {
        this.mRequestCode = mRequestCode;
        this.mPermission = mPermission;
    }

    public int getmRequestCode() {
        return mRequestCode;
    }

    public String getmPermission() {
        return mPermission;
    }
}
