package com.fastlib.app;

import android.app.Application;

import com.fastlib.bean.NetFlow;
import com.fastlib.db.FastDatabase;
import com.fastlib.net.NetManager;
import com.fastlib.utils.TimeUtil;

import java.util.Date;

/**
 * 全局环境配置
 */
public class FastApplication extends Application{
    public static final String NAME_SHAREPREFERENCES="fastlib";

    @Override
    public void onCreate() {
        super.onCreate();
        EventObserver.build(this);
    }

    /**
     * 保存一下流量使用情况，如果未使用不保存
     */
    private void saveNetFlow(){
        NetFlow netFlow =new NetFlow();
        netFlow.requestCount= NetManager.getInstance().mRequestCount;
        netFlow.receiveByte=NetManager.getInstance().Rx;
        netFlow.takeByte=NetManager.getInstance().Tx;
        netFlow.time= TimeUtil.dateToString(new Date(System.currentTimeMillis()));
        if(netFlow.requestCount>0)
            FastDatabase.getDefaultInstance(this).saveOrUpdate(netFlow);
    }
}