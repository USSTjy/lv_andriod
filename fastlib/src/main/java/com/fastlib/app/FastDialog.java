package com.fastlib.app;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

/**
 * Dialog集合
 */
public class FastDialog extends DialogFragment{
    public static final String ARG_TYPE="type";
    public static final String ARG_MESSAGE="message";
    public static final String ARG_DISPLAY_CANCEL="displayCancel";
    public static final String ARG_LIST="list";
    public static final String ARG_TITLE="title";
    public static final String ARG_POSITIVE_TITLE ="positiveMessage";
    public static final String ARG_NEGATIVE_TITLE ="nagativeMessage";
    public static final String TYPE_LIST="typeList";
    public static final String TYPE_MESSAGE="typeMessage";
    private DialogInterface.OnClickListener listener;
    private DialogInterface.OnClickListener mNegativeListener;

    public FastDialog setTitle(String title){
        Bundle bundle=getArguments();
        if(bundle==null)
            bundle=new Bundle();
        bundle.putString(ARG_TITLE,title);
        return this;
    }

    public static FastDialog showMessageDialog(String message,boolean displayCancel){
        FastDialog dialog=new FastDialog();
        Bundle bundle=new Bundle();

        bundle.putString(ARG_TYPE,TYPE_MESSAGE);
        bundle.putString(ARG_MESSAGE, message);
        bundle.putBoolean(ARG_DISPLAY_CANCEL, displayCancel);
        dialog.setArguments(bundle);
        return dialog;
    }

    public static FastDialog showListDialog(String[] items){
        FastDialog dialog=new FastDialog();
        Bundle bundle=new Bundle();

        bundle.putString(ARG_TYPE, TYPE_LIST);
        bundle.putStringArray(ARG_LIST, items);
        dialog.setArguments(bundle);
        return dialog;
    }

    public static FastDialog showListDialog(List<String> items){
        return showListDialog(items.toArray(new String[]{}));
    }

    public FastDialog setButtonTitle(String positiveTitle,String negativeTitle){
        getArguments().putString(ARG_POSITIVE_TITLE,positiveTitle);
        getArguments().putString(ARG_NEGATIVE_TITLE,negativeTitle);
        return this;
    }

    public void show(FragmentManager fm,DialogInterface.OnClickListener l){
        listener=l;
        show(fm,"dialog");
    }

    public void show(FragmentManager fm, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener){
        listener=positiveListener;
        mNegativeListener=negativeListener;
        show(fm,"dialog");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        final AlertDialog dialog;
        String s=getArguments().getString(ARG_TYPE);
        String positiveTitle=getArguments().getString(ARG_POSITIVE_TITLE,"确定");
        String negativeTitle=getArguments().getString(ARG_NEGATIVE_TITLE,"取消");
        if(TextUtils.isEmpty(s))
            s=ARG_MESSAGE;
        if(s.equals(TYPE_MESSAGE)){
            AlertDialog.Builder builder=new AlertDialog.Builder(getContext())
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(positiveTitle,listener);
            if(getArguments().getBoolean(ARG_DISPLAY_CANCEL))
                builder.setNegativeButton(negativeTitle,mNegativeListener);
            if(!TextUtils.isEmpty(getArguments().getString(ARG_TITLE)))
                builder.setTitle(getArguments().getString(ARG_TITLE));
            dialog=builder.create();
        }
        else{
            ListView lv=new ListView(getContext());
            ArrayAdapter<String> adapter=new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1,getArguments().getStringArray(ARG_LIST));
            lv.setAdapter(adapter);
            dialog=new AlertDialog
                    .Builder(getContext())
                    .setView(lv).create();
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(listener!=null)
                        listener.onClick(dialog,position);
                    dialog.dismiss();
                }
            });
        }
        return dialog;
    }
}