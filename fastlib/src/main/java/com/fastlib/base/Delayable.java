package com.fastlib.base;

/**
 * Created by sgfb on 16/2/25.
 * 可被延迟加载
 */
public interface Delayable{
    void startLoad();
}
