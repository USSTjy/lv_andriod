package com.wlbx.agent.bean;

import java.util.List;

public class BillboardListBean {


    private int code;
    private String msg;
    private DataDTO data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        private int count;
        private List<ListDTO> list;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<ListDTO> getList() {
            return list;
        }

        public void setList(List<ListDTO> list) {
            this.list = list;
        }

        public static class ListDTO {
            private int contractId;
            private int rank;
            private String branchAbbrName;
            private String memberName;
            private String productAbbrName;
            private double annualPrem;
            private int paymentPeriod;
            private String paymentPeriodType;
            private String paymentPeriodDesc;
            private String previewOrderImg;

            public int getContractId() {
                return contractId;
            }

            public void setContractId(int contractId) {
                this.contractId = contractId;
            }

            public int getRank() {
                return rank;
            }

            public void setRank(int rank) {
                this.rank = rank;
            }

            public String getBranchAbbrName() {
                return branchAbbrName;
            }

            public void setBranchAbbrName(String branchAbbrName) {
                this.branchAbbrName = branchAbbrName;
            }

            public String getMemberName() {
                return memberName;
            }

            public void setMemberName(String memberName) {
                this.memberName = memberName;
            }

            public String getProductAbbrName() {
                return productAbbrName;
            }

            public void setProductAbbrName(String productAbbrName) {
                this.productAbbrName = productAbbrName;
            }

            public double getAnnualPrem() {
                return annualPrem;
            }

            public void setAnnualPrem(double annualPrem) {
                this.annualPrem = annualPrem;
            }

            public int getPaymentPeriod() {
                return paymentPeriod;
            }

            public void setPaymentPeriod(int paymentPeriod) {
                this.paymentPeriod = paymentPeriod;
            }

            public String getPaymentPeriodType() {
                return paymentPeriodType;
            }

            public void setPaymentPeriodType(String paymentPeriodType) {
                this.paymentPeriodType = paymentPeriodType;
            }

            public String getPaymentPeriodDesc() {
                return paymentPeriodDesc;
            }

            public void setPaymentPeriodDesc(String paymentPeriodDesc) {
                this.paymentPeriodDesc = paymentPeriodDesc;
            }

            public String getPreviewOrderImg() {
                return previewOrderImg;
            }

            public void setPreviewOrderImg(String previewOrderImg) {
                this.previewOrderImg = previewOrderImg;
            }
        }
    }
}
