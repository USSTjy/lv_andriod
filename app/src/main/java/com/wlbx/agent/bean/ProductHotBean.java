package com.wlbx.agent.bean;


import java.util.List;

/**
 * 产品列表
 */
public class ProductHotBean {

    private int code;
    private String msg;
    private List<ListDTO> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ListDTO> getData() {
        return data;
    }

    public void setData(List<ListDTO> data) {
        this.data = data;
    }

    public static class ListDTO {
        private int productId;
        private String productName;
        private String productAbbrName;
        private String productAbbrDescription;
        private String productSubType;
        private String customLabel;
        private String logoUrl;
        private String backgroundUrl;
        private String productUrl;
        private String isOnline;
        private String needLogin;
        private String isHot;
        private String startPrice;
        private String partnerUrl;

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductAbbrName() {
            return productAbbrName;
        }

        public String getPartnerUrl() {
            return partnerUrl;
        }

        public void setPartnerUrl(String partnerUrl) {
            this.partnerUrl = partnerUrl;
        }

        public void setProductAbbrName(String productAbbrName) {
            this.productAbbrName = productAbbrName;
        }

        public String getProductAbbrDescription() {
            return productAbbrDescription;
        }

        public void setProductAbbrDescription(String productAbbrDescription) {
            this.productAbbrDescription = productAbbrDescription;
        }

        public String getProductSubType() {
            return productSubType;
        }

        public void setProductSubType(String productSubType) {
            this.productSubType = productSubType;
        }

        public String getCustomLabel() {
            return customLabel;
        }

        public void setCustomLabel(String customLabel) {
            this.customLabel = customLabel;
        }

        public String getLogoUrl() {
            return logoUrl;
        }

        public void setLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
        }

        public String getBackgroundUrl() {
            return backgroundUrl;
        }

        public void setBackgroundUrl(String backgroundUrl) {
            this.backgroundUrl = backgroundUrl;
        }

        public String getProductUrl() {
            return productUrl;
        }

        public void setProductUrl(String productUrl) {
            this.productUrl = productUrl;
        }

        public String getIsOnline() {
            return isOnline;
        }

        public void setIsOnline(String isOnline) {
            this.isOnline = isOnline;
        }

        public String getNeedLogin() {
            return needLogin;
        }

        public void setNeedLogin(String needLogin) {
            this.needLogin = needLogin;
        }

        public String getIsHot() {
            return isHot;
        }

        public void setIsHot(String isHot) {
            this.isHot = isHot;
        }

        public String getStartPrice() {
            return startPrice;
        }

        public void setStartPrice(String startPrice) {
            this.startPrice = startPrice;
        }
    }
}
