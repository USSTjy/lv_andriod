package com.wlbx.agent.bean;

import java.math.BigDecimal;

public class IncomeBean {

    private int code;
    private String msg;
    private DataDTO data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        private int totalCount;
        private BigDecimal totalAnnualPrem;
        private BigDecimal income;

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public BigDecimal getTotalAnnualPrem() {
            return totalAnnualPrem;
        }

        public void setTotalAnnualPrem(BigDecimal totalAnnualPrem) {
            this.totalAnnualPrem = totalAnnualPrem;
        }

        public BigDecimal getIncome() {
            return income;
        }

        public void setIncome(BigDecimal income) {
            this.income = income;
        }
    }
}
