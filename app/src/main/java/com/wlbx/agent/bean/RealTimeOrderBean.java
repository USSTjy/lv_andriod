package com.wlbx.agent.bean;

/**
 * 实时出单
 */
public class RealTimeOrderBean {
       private String contractId;
       private String rank;
       private String branchAbbrName;
       private String memberName;
       private String productAbbrName;
       private String annualPrem;
       private String paymentPeriod;
       private String paymentPeriodType;
       private String paymentPeriodDesc;
       private String previewOrderImg;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getBranchAbbrName() {
        return branchAbbrName;
    }

    public void setBranchAbbrName(String branchAbbrName) {
        this.branchAbbrName = branchAbbrName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getProductAbbrName() {
        return productAbbrName;
    }

    public void setProductAbbrName(String productAbbrName) {
        this.productAbbrName = productAbbrName;
    }

    public String getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(String paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public String getPaymentPeriodType() {
        return paymentPeriodType;
    }

    public void setPaymentPeriodType(String paymentPeriodType) {
        this.paymentPeriodType = paymentPeriodType;
    }

    public String getPaymentPeriodDesc() {
        return paymentPeriodDesc;
    }

    public void setPaymentPeriodDesc(String paymentPeriodDesc) {
        this.paymentPeriodDesc = paymentPeriodDesc;
    }

    public String getPreviewOrderImg() {
        return previewOrderImg;
    }

    public void setPreviewOrderImg(String previewOrderImg) {
        this.previewOrderImg = previewOrderImg;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getAnnualPrem() {
        return annualPrem;
    }

    public void setAnnualPrem(String annualPrem) {
        this.annualPrem = annualPrem;
    }
}
