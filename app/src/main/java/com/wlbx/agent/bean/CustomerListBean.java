package com.wlbx.agent.bean;

import java.util.List;

public class CustomerListBean {

    private int code;
    private String msg;
    private List<DataDTO> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        private int id;
        private String chnName;
        private int policyCount;
        private double sumPremium;
        private String birthdayRemindFlag;
        private String birthday;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getChnName() {
            return chnName;
        }

        public void setChnName(String chnName) {
            this.chnName = chnName;
        }

        public int getPolicyCount() {
            return policyCount;
        }

        public void setPolicyCount(int policyCount) {
            this.policyCount = policyCount;
        }

        public double getSumPremium() {
            return sumPremium;
        }

        public void setSumPremium(double sumPremium) {
            this.sumPremium = sumPremium;
        }

        public String getBirthdayRemindFlag() {
            return birthdayRemindFlag;
        }

        public void setBirthdayRemindFlag(String birthdayRemindFlag) {
            this.birthdayRemindFlag = birthdayRemindFlag;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }
    }
}
