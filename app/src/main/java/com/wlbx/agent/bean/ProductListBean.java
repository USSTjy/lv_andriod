package com.wlbx.agent.bean;


import java.io.Serializable;
import java.util.List;

/**
 * 产品列表
 */
public class ProductListBean {


    private int code;
    private String msg;
    private DataDTO data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        private int count;
        private List<ListDTO> list;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<ListDTO> getList() {
            return list;
        }

        public void setList(List<ListDTO> list) {
            this.list = list;
        }

        public static class ListDTO  implements Serializable {
            private int productId;
            private String productName;
            private String isOnline;
            private String isCarFlag;
            private String needLogin;
            private String productSubType;
            private String customLabel;
            private String logoUrl;
            private String backgroundUrl;
            private String productUrl;
            private String productAbbrDescription;
            private String partnerUrl;
            private String isHot;
            private String startPrice;

            public String getStartPrice() {
                return startPrice;
            }

            public void setStartPrice(String startPrice) {
                this.startPrice = startPrice;
            }

            public int getProductId() {
                return productId;
            }

            public void setProductId(int productId) {
                this.productId = productId;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getIsOnline() {
                return isOnline;
            }

            public void setIsOnline(String isOnline) {
                this.isOnline = isOnline;
            }

            public String getIsCarFlag() {
                return isCarFlag;
            }

            public void setIsCarFlag(String isCarFlag) {
                this.isCarFlag = isCarFlag;
            }

            public String getNeedLogin() {
                return needLogin;
            }

            public void setNeedLogin(String needLogin) {
                this.needLogin = needLogin;
            }

            public String getProductSubType() {
                return productSubType;
            }

            public void setProductSubType(String productSubType) {
                this.productSubType = productSubType;
            }

            public String getCustomLabel() {
                return customLabel;
            }

            public void setCustomLabel(String customLabel) {
                this.customLabel = customLabel;
            }

            public String getLogoUrl() {
                return logoUrl;
            }

            public void setLogoUrl(String logoUrl) {
                this.logoUrl = logoUrl;
            }

            public String getBackgroundUrl() {
                return backgroundUrl;
            }

            public void setBackgroundUrl(String backgroundUrl) {
                this.backgroundUrl = backgroundUrl;
            }

            public String getProductUrl() {
                return productUrl;
            }

            public void setProductUrl(String productUrl) {
                this.productUrl = productUrl;
            }

            public String getProductAbbrDescription() {
                return productAbbrDescription;
            }

            public void setProductAbbrDescription(String productAbbrDescription) {
                this.productAbbrDescription = productAbbrDescription;
            }

            public String getPartnerUrl() {
                return partnerUrl;
            }

            public void setPartnerUrl(String partnerUrl) {
                this.partnerUrl = partnerUrl;
            }

            public String getIsHot() {
                return isHot;
            }

            public void setIsHot(String isHot) {
                this.isHot = isHot;
            }
        }
    }
}
