package com.wlbx.agent.bean;

import java.io.Serializable;

/**
 * 代理人信息
 */
public class AgentBean implements Serializable {
    private  String accessToken;
    private  String publicKey;
    private String name;
    private String workNo;
    private String image;
    private String loginPwdFlag;
    private String mobile;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLoginPwdFlag() {
        return loginPwdFlag;
    }

    public void setLoginPwdFlag(String loginPwdFlag) {
        this.loginPwdFlag = loginPwdFlag;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
