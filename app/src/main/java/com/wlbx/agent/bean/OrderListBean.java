package com.wlbx.agent.bean;

import java.util.List;

public class OrderListBean {

    private int code;
    private String msg;
    private DataDTO data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        private int count;
        private List<ListDTO> list;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<ListDTO> getList() {
            return list;
        }

        public void setList(List<ListDTO> list) {
            this.list = list;
        }

        public static class ListDTO {
            private int contractId;
            private String orderNo;
            private String orderStatusName;
            private String policyHolderName;
            private String productName;
            private String payWayName;
            private String applicationDate;
            private double annualPrem;
            private String renewalFlag;
            private String reinsuranceFlag;

            public int getContractId() {
                return contractId;
            }

            public void setContractId(int contractId) {
                this.contractId = contractId;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public String getOrderStatusName() {
                return orderStatusName;
            }

            public void setOrderStatusName(String orderStatusName) {
                this.orderStatusName = orderStatusName;
            }

            public String getPolicyHolderName() {
                return policyHolderName;
            }

            public void setPolicyHolderName(String policyHolderName) {
                this.policyHolderName = policyHolderName;
            }

            public String getProductName() {
                return productName;
            }

            public void setProductName(String productName) {
                this.productName = productName;
            }

            public String getPayWayName() {
                return payWayName;
            }

            public void setPayWayName(String payWayName) {
                this.payWayName = payWayName;
            }

            public String getApplicationDate() {
                return applicationDate;
            }

            public void setApplicationDate(String applicationDate) {
                this.applicationDate = applicationDate;
            }

            public double getAnnualPrem() {
                return annualPrem;
            }

            public void setAnnualPrem(double annualPrem) {
                this.annualPrem = annualPrem;
            }

            public String getRenewalFlag() {
                return renewalFlag;
            }

            public void setRenewalFlag(String renewalFlag) {
                this.renewalFlag = renewalFlag;
            }

            public String getReinsuranceFlag() {
                return reinsuranceFlag;
            }

            public void setReinsuranceFlag(String reinsuranceFlag) {
                this.reinsuranceFlag = reinsuranceFlag;
            }
        }
    }
}
