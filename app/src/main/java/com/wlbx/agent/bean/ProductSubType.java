package com.wlbx.agent.bean;

import java.util.List;

/**
 * 产品类别
 */
public class ProductSubType {


    private int code;
    private String msg;
    private List<DataDTO> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        private String productSubType;
        private String productSubTypeName;
        private boolean isSelected;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public DataDTO() {
        }

        public DataDTO(String productSubType, String productSubTypeName) {
            this.productSubType = productSubType;
            this.productSubTypeName = productSubTypeName;
        }

        public String getProductSubType() {
            return productSubType;
        }

        public void setProductSubType(String productSubType) {
            this.productSubType = productSubType;
        }

        public String getProductSubTypeName() {
            return productSubTypeName;
        }

        public void setProductSubTypeName(String productSubTypeName) {
            this.productSubTypeName = productSubTypeName;
        }
    }
}
