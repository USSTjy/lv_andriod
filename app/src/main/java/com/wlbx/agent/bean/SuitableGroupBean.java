package com.wlbx.agent.bean;

import java.util.List;

/**
 * 适用人群
 */
public class SuitableGroupBean {


    private int code;
    private String msg;
    private List<DataDTO> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        private String suitableGroup;
        private String suitableGroupName;

        public DataDTO() {
        }

        public DataDTO(String suitableGroup, String suitableGroupName) {
            this.suitableGroup = suitableGroup;
            this.suitableGroupName = suitableGroupName;
        }

        public String getSuitableGroup() {
            return suitableGroup;
        }

        public void setSuitableGroup(String suitableGroup) {
            this.suitableGroup = suitableGroup;
        }

        public String getSuitableGroupName() {
            return suitableGroupName;
        }

        public void setSuitableGroupName(String suitableGroupName) {
            this.suitableGroupName = suitableGroupName;
        }
    }
}
