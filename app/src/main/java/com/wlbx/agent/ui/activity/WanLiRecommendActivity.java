package com.wlbx.agent.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.orhanobut.logger.Logger;
import com.wlbx.agent.adapter.vp.VpFragmentAdapter;
import com.wlbx.agent.base.BaseActivity;
import com.wlbx.agent.R;
import com.wlbx.agent.bean.SuitableGroupBean;
import com.wlbx.agent.global.Constant;
import com.wlbx.agent.net.RequestManager;
import com.wlbx.agent.net.RetrofitCallBack;
import com.wlbx.agent.net.RetrofitRequestInterface;
import com.wlbx.agent.ui.fragment.SubProductListFragment;
import com.wlbx.agent.view.ScaleTransitionPagerTitleView;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgePagerTitleView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 万利优选一级tab
 */
public class WanLiRecommendActivity extends BaseActivity {

    @BindView(R.id.tv_layout_top_back_title)
    TextView mTitleTV;
    @BindView(R.id.iv_layout_top_back)
    ImageView mBackIV;
    @BindView(R.id.magic_indicator)
    MagicIndicator magicIndicator;

    @BindView(R.id.vp_content)
    ViewPager mViewPager;

    private List<String> mDataList = new ArrayList<>();
    private List<String> mIndexDataList = new ArrayList<>();

    private List<SuitableGroupBean.DataDTO> mSuitableGroupList = new ArrayList<>();

    private String suitableGroup;
    @Override
    protected int getContentViewId() {
        return R.layout.activity_wan_li_recommend;
    }

    @Override
    public void initData() {
        mTitleTV.setText("万利优选");
        getSuitableGroup();
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.iv_layout_top_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_layout_top_back:
               finish();
                break;
            default:
                break;
        }
    }


    public void getSuitableGroup() {
        final Map<String, String> map = new HashMap<>();
        map.put("suitableGroup","");//适用人群类型
        map.put("suitableGroupName","");//适用人群名称
        Logger.i(JSON.toJSONString(map));
        RequestManager
                .mRetrofitManager
                .createRequest(RetrofitRequestInterface.class)
                .getSuitableGroup(map)
                .enqueue(new RetrofitCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        Logger.i("适用人群----" +response);
                        JSONObject jsonObject = JSON.parseObject(response);
                        String rCode = jsonObject.getString("code");
                        String rMsg  = jsonObject.getString("msg");
                        if("1000".equals(rCode)) {
                        SuitableGroupBean bean= JSON.parseObject(response,SuitableGroupBean.class);
                            mSuitableGroupList = bean.getData();

                            mSuitableGroupList.add(0,new SuitableGroupBean.DataDTO("","全部"));

                            for (SuitableGroupBean.DataDTO dataDTO : mSuitableGroupList) {
                                mDataList.add(dataDTO.getSuitableGroupName());
                                mIndexDataList.add(dataDTO.getSuitableGroup());
                            }
                            initTabAndPager(0);
                        }else{
                            Toast.makeText(WanLiRecommendActivity.this,rMsg,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Logger.i( t.getMessage());


                    }
                });
    }
    Bundle bundle;
    private void initTabAndPager(int item) {
        List<Fragment> fragments = new ArrayList<>();
        for (int i = 0; i < mSuitableGroupList.size(); i++) {
            SubProductListFragment fragment = new SubProductListFragment();
            bundle = new Bundle();
            bundle.putString(Constant.BUNDLE_KEY_PRODUCT_GROUP, mSuitableGroupList.get(i).getSuitableGroup());//筛选适用人群
            fragment.setArguments(bundle);
            fragments.add(i, fragment);
        }
        VpFragmentAdapter mAdapter =new VpFragmentAdapter(((AppCompatActivity) this).getSupportFragmentManager(), fragments);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(fragments.size());
        mViewPager.setCurrentItem(item, false);

        initMagicIndicator();
    }





    private void initMagicIndicator() {
        magicIndicator.setBackgroundColor(Color.WHITE);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(false);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override

            public IPagerTitleView getTitleView(Context context, final int index) {

                SimplePagerTitleView simplePagerTitleView = new ScaleTransitionPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setTextSize(18);
                simplePagerTitleView.setNormalColor(Color.parseColor("#828282"));
                simplePagerTitleView.setSelectedColor(Color.parseColor("#F68657"));
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                        suitableGroup= mSuitableGroupList.get(index).getSuitableGroup();
//                        Logger.i(mSuitableGroupList.get(index).getSuitableGroup()+"\n"+mSuitableGroupList.get(index).getSuitableGroupName());
                    }
                });

                BadgePagerTitleView badgePagerTitleView = new BadgePagerTitleView(context);
                badgePagerTitleView.setInnerPagerTitleView(simplePagerTitleView);
                return badgePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
                indicator.setLineHeight(UIUtil.dip2px(context, 4));
                indicator.setLineWidth(UIUtil.dip2px(context, 20));
                indicator.setRoundRadius(UIUtil.dip2px(context, 3));
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(2.0f));
                indicator.setColors(Color.parseColor("#F68657"));


                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

}