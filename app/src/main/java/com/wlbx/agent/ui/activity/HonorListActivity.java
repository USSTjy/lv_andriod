package com.wlbx.agent.ui.activity;

import com.wlbx.agent.R;
import com.wlbx.agent.base.BaseActivity;

/**
 * 榜单
 */
public class HonorListActivity extends BaseActivity {

    @Override
    protected int getContentViewId() {
        return R.layout.activity_honor_list;
    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initData() {

    }
}