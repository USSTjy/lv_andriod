package com.wlbx.agent.ui.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.luck.picture.lib.tools.ToastUtils;
import com.orhanobut.logger.Logger;
import com.wlbx.agent.R;
import com.wlbx.agent.base.BaseActivity;
import com.wlbx.agent.event.UpdateUserInfoEvent;
import com.wlbx.agent.global.Constant;
import com.wlbx.agent.net.RequestManager;
import com.wlbx.agent.net.RetrofitCallBack;
import com.wlbx.agent.net.RetrofitRequestInterface;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessageActivity extends BaseActivity {
    private static final String TAG = "MessageActivity";

    @BindView(R.id.tv_layout_top_back_title)
    TextView mTitleTV;
    @BindView(R.id.iv_layout_top_back)
    ImageView mBackIV;
    @BindView(R.id.tv_read)
    TextView mReadTV;
    @BindView(R.id.ll_type1)
    LinearLayout ll_type1;
    @BindView(R.id.ll_type3)
    LinearLayout ll_type3;
    @BindView(R.id.iv_tip1)
    ImageView mTip1IV;
    @BindView(R.id.iv_tip3)
    ImageView mTip3IV;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_message;
    }

    @Override
    public void initData() {
        mTitleTV.setText("消息");

    }

    @Override
    protected void onResume() {
        super.onResume();
        getMessage();
    }

    @Override
    public void initEvent() {}

    @OnClick({R.id.tv_read,R.id.iv_layout_top_back,R.id.ll_type1,R.id.ll_type3})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_read:
                setReaded();
                break;
            case R.id.iv_layout_top_back:
                finish();
                break;
            case R.id.ll_type1:
                //平台消息
                toMessageList("01");
                break;
            case R.id.ll_type3:
                //我的消息
                toMessageList("03");
                break;
            default:
                break;
        }
    }

    //跳转到消息页面
    public void toMessageList(String type){
        Intent intent = new Intent(this,MessageListActivity.class);
        intent.putExtra("type",type);
        startActivity(intent);
    }

    //获取未读消息
    public void getMessage(){
        RequestManager
        .mRetrofitManager
        .createRequest(RetrofitRequestInterface.class)
        .getUnReadMessage()
        .enqueue(new RetrofitCallBack() {
            @Override
            public void onSuccess(String response) {
                Logger.i("初始化---"+response);
                JSONObject jsonObject = JSON.parseObject(response);
                String rCode = jsonObject.getString("code");
                String rMsg  = jsonObject.getString("msg");
                if("1000".equals(rCode)) {
                    init(jsonObject);
                }else{
                    Toast.makeText(mContext,rMsg,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onError(Throwable t) {
                Logger.i(TAG, t.getMessage());
                Toast.makeText(mContext,"获取未读消息失败"+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    //初始界面数据
    public void init(JSONObject jsonObject){
        if(jsonObject!=null) {
            JSONArray jarr = jsonObject.getJSONArray("data");
            for (int i = 0; i < jarr.size(); i++) {
                JSONObject jsono = (JSONObject) jarr.get(i);
                //01平台，03我的消息，04产品动态
                if ("01".equals(getStringValue(jsono, "messageType"))) {
                    if (jsono.getInteger("unreadCount") > 0) {
                        mTip1IV.setVisibility(View.VISIBLE);
                    } else {
                        mTip1IV.setVisibility(View.GONE);
                    }
                }
                if ("03".equals(getStringValue(jsono, "messageType"))) {
                    if (jsono.getInteger("unreadCount") > 0) {
                        mTip3IV.setVisibility(View.VISIBLE);
                    } else {
                        mTip3IV.setVisibility(View.GONE);
                    }
                }
            }
        }else{
            mTip1IV.setVisibility(View.GONE);
            mTip3IV.setVisibility(View.GONE);
        }
    }

    //设置已读
    public void setReaded() {
        RequestManager
        .mRetrofitManager
        .createRequest(RetrofitRequestInterface.class)
        .setReaded()
        .enqueue(new RetrofitCallBack() {
            @Override
            public void onSuccess(String response) {
                Logger.i("setRead"+ response);
                JSONObject jsonObject = JSON.parseObject(response);
                String rCode = jsonObject.getString("code");
                String rMsg  = jsonObject.getString("msg");
                if("1000".equals(rCode)) {
                    init(null);
                    UpdateUserInfoEvent updateUserInfoEvent=new UpdateUserInfoEvent();
                    EventBus.getDefault().postSticky( updateUserInfoEvent);
                    ToastUtils.s(MessageActivity.this,rMsg);
                }else{
                    Toast.makeText(mContext,rMsg,Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onError(Throwable t) {
                Logger.i(TAG, t.getMessage());
                Toast.makeText(mContext,"设置已读失败"+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getStringValue(JSONObject json, String key){
        String ret = "";
        try{
            ret = json.getString(key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ret;
    }
}