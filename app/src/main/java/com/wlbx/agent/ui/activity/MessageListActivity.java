package com.wlbx.agent.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.orhanobut.logger.Logger;
import com.wlbx.agent.R;
import com.wlbx.agent.adapter.MessageAdapter;
import com.wlbx.agent.base.BaseActivity;
import com.wlbx.agent.bean.MessageListBean;
import com.wlbx.agent.global.AccountManager;
import com.wlbx.agent.net.RequestManager;
import com.wlbx.agent.net.RetrofitCallBack;
import com.wlbx.agent.net.RetrofitRequestInterface;
import com.wlbx.agent.util.IntentUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import butterknife.BindView;
import butterknife.OnClick;

public class MessageListActivity extends BaseActivity {
    private static final String TAG = "MessageListActivity";

    @BindView(R.id.tv_layout_top_back_title)
    TextView mTitleTV;
    @BindView(R.id.iv_layout_top_back)
    ImageView mBackIV;
    @BindView(R.id.rv_message)
    RecyclerView mMesssageRV;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;

    MessageAdapter mAdapter;
    List<MessageListBean.DataDTO.ListDTO> data;

    private String type = "";
    private static final int PAGE_SIZE = 10;

    Bundle bundle=new Bundle();
    private PageInfo pageInfo = new PageInfo();

    static class PageInfo {
        int page = 1;

        void nextPage() {
            page++;
        }

        void reset() {
            page = 1;
        }

        boolean isFirstPage() {
            return page == 1;
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_messagelist;
    }

    @Override
    public void initData() {
        if (AccountManager.hasUserLogin()){
            type = getIntent().getStringExtra("type");
            Logger.i("type----"+type);
            if("01".equals(type)){mTitleTV.setText("平台消息");}
            else if("03".equals(type)){mTitleTV.setText("我的消息");}
            initAdapter();
        }else {
            IntentUtil.goLoginActivity(MessageListActivity.this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        pageInfo.reset();
        getMessageList(type,pageInfo.page);

    }

    @Override
    public void initEvent() {}

    @OnClick({R.id.iv_layout_top_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_layout_top_back:
                finish();
                break;
            default:
                break;
        }
    }

    public void initAdapter(){
        mAdapter = new MessageAdapter(this);
        mMesssageRV.setLayoutManager(new LinearLayoutManager(this));
        mMesssageRV.setAdapter(mAdapter);
        mAdapter.setAnimationEnable(true);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageInfo.reset();
                getMessageList(type,pageInfo.page);
                mAdapter.getLoadMoreModule().setEnableLoadMore(false);
            }
        });

        mAdapter.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                getMessageList(type,pageInfo.page);
            }
        });
    }

    //跳转到消息页面
    public void getMessageList(String type,int page){
        final Map<String, String> map = new HashMap<>();
        map.put("curPage",String.valueOf(page));
        map.put("length",String.valueOf(PAGE_SIZE));
        map.put("messageType",type);
        Logger.i(TAG, JSON.toJSONString(map));
        RequestManager
        .mRetrofitManager
        .createRequest(RetrofitRequestInterface.class)
        .getMessageList(JSON.toJSONString(map))
        .enqueue(new RetrofitCallBack() {
            @Override
            public void onSuccess(String response) {
                Logger.i(response);
                JSONObject jsonObject = JSON.parseObject(response);
                String rCode = jsonObject.getString("code");
                String rMsg  = jsonObject.getString("msg");
                if("1000".equals(rCode)) {
                    if (swipeLayout!=null){
                        swipeLayout.setRefreshing(false);
                    }
                    if (mAdapter!=null){
                        mAdapter.getLoadMoreModule().setEnableLoadMore(true);
                    }
                    MessageListBean bean=JSON.parseObject(response,MessageListBean.class);
                    data = bean.getData().getList();
                    if (data.size()<=0||data==null){
                        mAdapter.setEmptyView(R.layout.no_message_view);
                    }
                    if (pageInfo.isFirstPage()) {
                        mAdapter.setList(data);
                    } else {
                        mAdapter.addData(data);
                    }
                    if (data.size() < PAGE_SIZE) {
                        mAdapter.getLoadMoreModule().loadMoreEnd();
                    } else {
                        mAdapter.getLoadMoreModule().loadMoreComplete();
                    }
                    pageInfo.nextPage();
                }else {
                    Toast.makeText(mActivity, rMsg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Throwable t) {
                Logger.i(TAG, t.getMessage());
                Toast.makeText(mContext,"查询消息失败"+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

}