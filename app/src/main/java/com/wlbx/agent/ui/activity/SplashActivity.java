package com.wlbx.agent.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.wlbx.agent.R;
import com.wlbx.agent.base.BaseActivity;
import com.wlbx.agent.global.Constant;
import com.wlbx.agent.util.SPUtil;
import com.orhanobut.logger.Logger;

import butterknife.BindView;

public class SplashActivity extends BaseActivity {

    private static final String TAG = "SplashActivity";

    @BindView(R.id.activity_splash)
    RelativeLayout mRelativeLayout;


    @Override
    protected int getContentViewId() {
        return R.layout.activity_splash;
    }

    @Override
    public void initData() {
//        //防止在Application拿不到激光的registrationId
//        String registrationId = JPushInterface.getRegistrationID(getApplicationContext());
//        Logger.i(TAG, "闪屏页---registrationId---" + registrationId);
//        SPUtil.put(Constant.DEVICE_TOKEN, registrationId);
    }

    @Override
    public void initEvent() {

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation.setDuration(1000);
        mRelativeLayout.startAnimation(alphaAnimation);
        //设置动画监听
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            //动画结束
            @Override
            public void onAnimationEnd(Animation animation) {

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    String[] mPermissionList = new String[]{
//                             Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
//                    };
//                    ActivityCompat.requestPermissions(mActivity, mPermissionList, 200);
//                } else {
//
                    freeForLogin();

//                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }


    /**
     * 处理权限申请回调
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case 200:
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    freeForLogin();
//                } else {
//                    Toast.makeText(mActivity, "权限被拒绝，请手动开启权限", Toast.LENGTH_SHORT).show();
//                }
//                break;
//            default:
//                break;
//        }

    }


    /**
     * 免登陆
     */
    private void freeForLogin() {
//        String token = SPUtil.getValue(Constant.USER_INFO, Constant.TOKEN);
//        Logger.i(TAG, "token---" + token);
       /* if (TextUtils.isEmpty(token)) {
            //页面的跳转
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
            SplashActivity.this.finish();
        } else {*/
            //页面的跳转
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            SplashActivity.this.finish();
//        }
    }


}
