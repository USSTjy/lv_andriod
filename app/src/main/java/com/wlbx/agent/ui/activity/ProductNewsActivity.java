package com.wlbx.agent.ui.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnLoadMoreListener;
import com.orhanobut.logger.Logger;
import com.wlbx.agent.R;
import com.wlbx.agent.adapter.ProductNewsAdapter;
import com.wlbx.agent.base.BaseActivity;
import com.wlbx.agent.bean.ProductNewsBean;
import com.wlbx.agent.net.RequestManager;
import com.wlbx.agent.net.RetrofitCallBack;
import com.wlbx.agent.net.RetrofitRequestInterface;
import com.wlbx.agent.util.IntentUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * 产品动态
 */
public class ProductNewsActivity extends BaseActivity {

    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    ProductNewsAdapter mAdapter;

    @BindView(R.id.tv_layout_top_back_title)
    TextView mTitleTV;
    @BindView(R.id.iv_layout_top_back)
    ImageView mBackIV;
    private static final int PAGE_SIZE = 10;

    private PageInfo pageInfo = new PageInfo();

    static class PageInfo {
        int page = 1;

        void nextPage() {
            page++;
        }

        void reset() {
            page = 1;
        }

        boolean isFirstPage() {
            return page == 1;
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_product_news;
    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initData() {
        mTitleTV.setText("产品动态");
        initAdapter();
//        getList(pageInfo.page);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pageInfo.reset();
        getList(pageInfo.page);
    }

    private void initAdapter() {
        mAdapter = new ProductNewsAdapter();
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(mAdapter);

        mAdapter.setAnimationEnable(true);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageInfo.reset();
                getList(pageInfo.page);
                mAdapter.getLoadMoreModule().setEnableLoadMore(false);
            }
        });

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                ProductNewsBean.DataDTO.ListDTO bean = mAdapter.getData().get(position);
                IntentUtil.goProductDetailActivity(bean.getSendTime(),bean.getReadFlag(),bean.getId(),bean.getProductId(),bean.getMessageTitle(),bean.getMessageContent(),bean.getProductUrl(),ProductNewsActivity.this);
//                setSingleRead(bean.getId());
            }
        });

        mAdapter.getLoadMoreModule().setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                getList(pageInfo.page);
            }
        });

    }

    private void setSingleRead(int id) {
        RequestManager
                .mRetrofitManager
                .createRequest(RetrofitRequestInterface.class)
                .setSingleRead( id)
                .enqueue(new RetrofitCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        Logger.i( response);
                        JSONObject jsonObject = JSON.parseObject(response);
                        String rCode = jsonObject.getString("code");
                        String rMsg  = jsonObject.getString("msg");
                        if("1000".equals(rCode)) {
                            pageInfo.reset();
                            getList(pageInfo.page);
                        }else{
                            Toast.makeText(ProductNewsActivity.this,rMsg,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Logger.i( t.getMessage());

                    }
                });
    }

    List<ProductNewsBean.DataDTO.ListDTO> data;

    public void getList(int page) {
        final Map<String, String> map = new HashMap<>();
        map.put("curPage",String.valueOf(page));
        map.put("length",String.valueOf(PAGE_SIZE));
        Logger.i(JSON.toJSONString(map));
        RequestManager
                .mRetrofitManager
                .createRequest(RetrofitRequestInterface.class)
                .productTrendList(JSON.toJSONString(map))
                .enqueue(new RetrofitCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        Logger.i( response);
                        JSONObject jsonObject = JSON.parseObject(response);
                        String rCode = jsonObject.getString("code");
                        String rMsg  = jsonObject.getString("msg");
                        if("1000".equals(rCode)) {
                        ProductNewsBean bean=JSON.parseObject(response,ProductNewsBean.class);
                        if (swipeLayout!=null){
                            swipeLayout.setRefreshing(false);
                        }
                            mAdapter.getLoadMoreModule().setEnableLoadMore(true);
                            data = bean.getData().getList();
                            if (data.size()<=0||data==null){
                                mAdapter.setEmptyView(R.layout.no_data_view);
                            }

                        if (pageInfo.isFirstPage()) {
                            mAdapter.setList(data);
                        } else {
                            mAdapter.addData(data);
                        }

                        if (data.size() < PAGE_SIZE) {
                            mAdapter.getLoadMoreModule().loadMoreEnd();
                        } else {
                            mAdapter.getLoadMoreModule().loadMoreComplete();
                        }

                        pageInfo.nextPage();
                        }else{
                            Toast.makeText(ProductNewsActivity.this,rMsg,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Logger.i( t.getMessage());

                    }
                });
    }


    @OnClick({R.id.iv_layout_top_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_layout_top_back:
                finish();
                break;
            default:
                break;
        }
    }

}