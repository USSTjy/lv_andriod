package com.wlbx.agent.ui.fragment.tab;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.orhanobut.logger.Logger;
import com.wlbx.agent.R;
import com.wlbx.agent.adapter.ProductGridViewAdapter;
import com.wlbx.agent.base.BaseFragment;
import com.wlbx.agent.bean.AgentBean;
import com.wlbx.agent.bean.ProductHotBean;
import com.wlbx.agent.bean.ProductListBean;
import com.wlbx.agent.bean.RealTimeOrderBean;
import com.wlbx.agent.event.RefreshHomePageEvent;
import com.wlbx.agent.event.UpdateFilterEvent;
import com.wlbx.agent.event.UpdateOnlineLayoutEvent;
import com.wlbx.agent.event.UpdateProductListEvent;
import com.wlbx.agent.global.AccountManager;
import com.wlbx.agent.global.Constant;
import com.wlbx.agent.net.RequestManager;
import com.wlbx.agent.net.RetrofitCallBack;
import com.wlbx.agent.net.RetrofitRequestInterface;
import com.wlbx.agent.ui.activity.LoginActivity;
import com.wlbx.agent.ui.activity.MainActivity;
import com.wlbx.agent.ui.activity.MessageActivity;
import com.wlbx.agent.ui.activity.WebActivity;
import com.wlbx.agent.util.ActivityUtil;
import com.wlbx.agent.util.Constants;
import com.wlbx.agent.util.IntentUtil;
import com.wlbx.agent.util.SPUtil;
import com.wlbx.agent.view.GridViewForScrollView;
import com.wlbx.agent.view.customscrollview.CustomScrollView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeFragment extends BaseFragment {
    private static final String TAG = "HomeFragment";
    @BindView(R.id.tb_tip)
    ImageButton tb_tip;
    @BindView(R.id.sv_data)
    CustomScrollView sv_data;
    @BindView(R.id.vp_tt_roll)
    ViewFlipper vp;
    @BindView(R.id.ll_top)
    LinearLayout ll_top;
    @BindView(R.id.ll_top1)
    LinearLayout ll_top1;
    @BindView(R.id.ll_top2)
    LinearLayout ll_top2;
    @BindView(R.id.ll_top3)
    LinearLayout ll_top3;
    @BindView(R.id.ll_top_other)
    LinearLayout ll_top_other;
    @BindView(R.id.gv_home_product)
    GridViewForScrollView gv_home_product;
    @BindView(R.id.ll_label)
    LinearLayout ll_label;
    @BindView(R.id.tv_name1)
    TextView tv_name1;
    @BindView(R.id.tv_desc1)
    TextView tv_desc1;
    @BindView(R.id.tv_fee1)
    TextView tv_fee1;
    @BindView(R.id.iv_bk1)
    ImageView iv_bk1;
    @BindView(R.id.tv_name2)
    TextView tv_name2;
    @BindView(R.id.tv_desc2)
    TextView tv_desc2;
    @BindView(R.id.tv_fee2)
    TextView tv_fee2;
    @BindView(R.id.iv_bk2)
    ImageView iv_bk2;
    @BindView(R.id.tv_name3)
    TextView tv_name3;
    @BindView(R.id.tv_desc3)
    TextView tv_desc3;
    @BindView(R.id.tv_fee3)
    TextView tv_fee3;
    @BindView(R.id.iv_bk3)
    ImageView iv_bk3;
    @BindView(R.id.rl_top1)
    RelativeLayout rl_top1;
    @BindView(R.id.rl_top2)
    RelativeLayout rl_top2;
    @BindView(R.id.rl_top3)
    RelativeLayout rl_top3;
    @BindView(R.id.wr_fresh)
    SwipeRefreshLayout  wr_fresh;
    @BindView(R.id.rl_searchbar)
    RelativeLayout rlSearchbar;
    @BindView(R.id.ll_oper1)
    LinearLayout ll_oper1;
    @BindView(R.id.ll_oper2)
    LinearLayout ll_oper2;
    @BindView(R.id.ll_oper3)
    LinearLayout ll_oper3;
    @BindView(R.id.ll_oper4)
    LinearLayout ll_oper4;
    @BindView(R.id.iv_tips)
    ImageView iv_tip;
    @BindView(R.id.iv_producttip)
    ImageView iv_producttip;
    @BindView(R.id.tv_tip1)
    TextView tv_tip1;
    @BindView(R.id.tv_tip2)
    TextView tv_tip2;
    @BindView(R.id.tv_tip3)
    TextView tv_tip3;

    private ProductGridViewAdapter productGridViewAdapter;
    private PopupWindow mPopupProductTipWindow;
    private List<RealTimeOrderBean> list = new ArrayList<RealTimeOrderBean> ();
    private List<ProductHotBean.ListDTO> alldata = new ArrayList<ProductHotBean.ListDTO>();

    @BindView(R.id.ll_menu)
    LinearLayout llMenu;

    @BindView( R.id.ib_oper3)
    ImageButton ibOper3;

    @BindView( R.id.ib_oper4)
    ImageButton ibOper4;

    @BindView(R.id.tv_keyword)
    TextView tvKeyword;

    @Override
    protected int getInflateViewId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initData() {
        if (!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this);
        }
        // 判断是否是第一次开启应用
        boolean isFirstOpen = SPUtil.getBoolean(Constant.USER_INFO, Constant.FIRST_OPEN);
        // 如果是第一次启动，则先进入功能引导页
        if (!isFirstOpen) {
            showPopupWindow();
        }

        sv_data.setOnScrollListener(
                new CustomScrollView.OnScrollListener(){
                    @Override
                    public void onScroll(int scrollY) {
                        wr_fresh.setEnabled(scrollY == 0);
                    }
                }
        );
        vp.setFlipInterval(5000);
        vp.setInAnimation(mActivity,R.anim.anim_in_v);
        vp.setOutAnimation(mActivity,R.anim.anim_out_v);
        getBillBoard();
        getHotProducts();
        wr_fresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBillBoard();
                getHotProducts();
                fresh_message();
                wr_fresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        fresh_message();
    }

    @Override
    public void initEvent() {

    }

    @OnClick({R.id.tb_tip,R.id.rl_top1,R.id.rl_top2,R.id.rl_top3,R.id.ll_top1,R.id.ll_top2,R.id.ll_top3,R.id.rl_searchbar,R.id.ib_oper1,R.id.ib_oper2,R.id.ib_oper3,R.id.ll_menu,R.id.ib_oper4})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_tip:
                if(!checkLogin()){
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                }else {
                    startActivity(new Intent(this.mActivity, MessageActivity.class));
                }
                break;
            case R.id.ll_top1:
                if (AccountManager.hasUserLogin()) {
                    try{
                        ProductListBean.DataDTO.ListDTO bean = new ProductListBean.DataDTO.ListDTO();
                        bean = copyProperties(bean,alldata.get(0));
                        toDetail(bean);
                    }catch (Exception e){ e.printStackTrace();}
                }
                else {
                    IntentUtil.goLoginActivity(getActivity());
                }
                break;
            case R.id.ll_top2:
                if (AccountManager.hasUserLogin()) {
                    try{
                        ProductListBean.DataDTO.ListDTO bean = new ProductListBean.DataDTO.ListDTO();
                        bean = copyProperties(bean,alldata.get(1));
                        toDetail(bean);
                    }catch (Exception e){ e.printStackTrace();}
                }else {
                    IntentUtil.goLoginActivity(getActivity());
                }
                break;
            case R.id.ll_top3:
                if (AccountManager.hasUserLogin()) {
                    try{
                        ProductListBean.DataDTO.ListDTO bean = new ProductListBean.DataDTO.ListDTO();
                        bean = copyProperties(bean,alldata.get(2));
                        toDetail(bean);
                    }catch (Exception e){ e.printStackTrace(); }
                }
                 else {
                        IntentUtil.goLoginActivity(getActivity());
                    }
                break;
            case R.id.rl_searchbar:
                IntentUtil.goSearchActivity(getActivity(),"home");
                break;
            case R.id.ib_oper1:
                toOper1("N");
                break;
            case R.id.ib_oper2:
                toOper2("Y");
                break;
            case R.id.ib_oper3:
                IntentUtil.goWanLiRecommendActivity(getActivity());
                break;
            case R.id.ll_menu:
                IntentUtil.goBillboardActivity(getActivity());
                break;
            case R.id.ib_oper4:
                if(!checkLogin()){
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                }else {
                    IntentUtil.goProductNewsActivity(getActivity());
                }
                break;
            default:
                break;
        }
    }



    public void toSearch(){
        Intent intent = new Intent(mActivity,WebActivity.class);
        intent.putExtra("purl", Constants.searchUrl);
        startActivity(intent);
    }


    public void toOper1(String isOnline){
        MainActivity mainActivity= (MainActivity) getActivity();
        mainActivity.toTab2(isOnline);
        UpdateOnlineLayoutEvent updateOnlineLayoutEvent=new UpdateOnlineLayoutEvent();
        Constant.IS_ONLINE=isOnline;
        updateOnlineLayoutEvent.setIsOnline(Constant.IS_ONLINE);
        EventBus.getDefault().postSticky(updateOnlineLayoutEvent);
        UpdateFilterEvent updateFilterEvent =new UpdateFilterEvent();
        EventBus.getDefault().postSticky(updateFilterEvent);
    }


    public void toOper2(String isOnline){
        MainActivity mainActivity= (MainActivity) getActivity();
        mainActivity.toTab2(isOnline);
        UpdateOnlineLayoutEvent updateOnlineLayoutEvent=new UpdateOnlineLayoutEvent();
        Constant.IS_ONLINE="Y";
        updateOnlineLayoutEvent.setIsOnline(Constant.IS_ONLINE);
        EventBus.getDefault().postSticky(updateOnlineLayoutEvent);
        UpdateFilterEvent updateFilterEvent =new UpdateFilterEvent();
        EventBus.getDefault().postSticky(updateFilterEvent);
    }

    public void toDetail(ProductListBean.DataDTO.ListDTO bean){
        Intent intent = new Intent(mActivity, WebActivity.class);
        intent.putExtra("bean",bean);
        startActivity(intent);
    }

    public void toDetailUrl(String url){
        Intent intent = new Intent(mActivity, WebActivity.class);
        intent.putExtra("purl",url);
        startActivity(intent);
    }

    public void getBillBoard(){
        RequestManager
        .mRetrofitManager
        .createRequest(RetrofitRequestInterface.class)
        .getBillBoard()
        .enqueue(new RetrofitCallBack() {
            @Override
            public void onSuccess(String response) {
                Logger.i("榜单"+response);

                JSONObject jsonObject = JSON.parseObject(response);
                String rCode = jsonObject.getString("code");
                String rMsg  = jsonObject.getString("msg");
                if("1000".equals(rCode)) {
                    list = getRealTimeOrder(jsonObject);
                    fresh_vp(list);
                }else {
                    Toast.makeText(mActivity, rMsg, Toast.LENGTH_SHORT).show();
                }
            }

                    @Override
                    public void onError(Throwable t) {
                        Logger.i(TAG, t.getMessage());
                        Toast.makeText(mActivity,"获取榜单数据失败"+t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //获取热销产品
    public void getHotProducts(){
        RequestManager
        .mRetrofitManager
        .createRequest(RetrofitRequestInterface.class)
        .getHotProducts()
        .enqueue(new RetrofitCallBack() {
            @Override
            public void onSuccess(String response) {
                JSONObject jsonObject = JSON.parseObject(response);
                String rCode = jsonObject.getString("code");
                String rMsg  = jsonObject.getString("msg");
                if("1000".equals(rCode)) {
                    ProductHotBean bean=JSON.parseObject(response,ProductHotBean.class);
                    alldata = bean.getData();
                    //获取前面3个产品展示
                    if(alldata.size()>=3) {
						ll_top.setVisibility(View.VISIBLE);
						showHotTop(alldata.subList(0,3));
					}else{
						ll_top.setVisibility(View.GONE);
					}
                    //后面产品放GridView展示，每行2个产品
                    if(alldata.size()>3) showGridView(alldata.subList(3,alldata.size()));
                }else {
                    Toast.makeText(mActivity, rMsg, Toast.LENGTH_SHORT).show();
                }
            }

                    @Override
                    public void onError(Throwable t) {
                        Logger.i(TAG, t.getMessage());
                        Toast.makeText(mActivity,"获取热销产品失败"+t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
    }
    //首页展示3款产品
    public void showHotTop(List<ProductHotBean.ListDTO> data){
        tv_name1.setText(data.get(0).getProductAbbrName());
        tv_desc1.setText(data.get(0).getProductAbbrDescription());
        tv_fee1.setText(data.get(0).getStartPrice());
        if("Y".equals(data.get(0).getIsHot())){
            tv_tip1.setVisibility(View.VISIBLE);
        }else{
            tv_tip1.setVisibility(View.INVISIBLE);
        }
        tv_name2.setText(data.get(1).getProductAbbrName());
        tv_desc2.setText(data.get(1).getProductAbbrDescription());
        tv_fee2.setText(data.get(1).getStartPrice());
        if("Y".equals(data.get(1).getIsHot())){
            tv_tip2.setVisibility(View.VISIBLE);
        }else{
            tv_tip2.setVisibility(View.INVISIBLE);
        }
        tv_name3.setText(data.get(2).getProductAbbrName());
        tv_desc3.setText(data.get(2).getProductAbbrDescription());
        tv_fee3.setText(data.get(2).getStartPrice());
        if("Y".equals(data.get(2).getIsHot())){
            tv_tip3.setVisibility(View.VISIBLE);
        }else{
            tv_tip3.setVisibility(View.INVISIBLE);
        }
        fillLabel(ll_label,data.get(0).getCustomLabel());
        Glide.with(iv_bk1.getContext()).load(data.get(0).getBackgroundUrl()).into(iv_bk1);
        Glide.with(iv_bk2.getContext()).load(data.get(1).getBackgroundUrl()).into(iv_bk2);
        Glide.with(iv_bk3.getContext()).load(data.get(2).getBackgroundUrl()).into(iv_bk3);
    }
    //首页展示产品列表
    public void showGridView(List<ProductHotBean.ListDTO> data){
        productGridViewAdapter = new ProductGridViewAdapter(getContext(),data);
        gv_home_product.setAdapter(productGridViewAdapter);
        gv_home_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (AccountManager.hasUserLogin()) {
                    try{
                        ProductListBean.DataDTO.ListDTO bean = new ProductListBean.DataDTO.ListDTO();
                        bean = copyProperties(bean,data.get(i));
                        toDetail(bean);
                    }catch (Exception e){ e.printStackTrace();}
                }else {
                    IntentUtil.goLoginActivity(getActivity());
                }
            }
        });
    }


    //刷新VP的数据
    public void fresh_vp(List<RealTimeOrderBean> list){
        if(list.size() != 0) {
            if (vp != null) {
                vp.stopFlipping();
                if ((list.size()) % 2 == 1) list.add(list.get(0));
                for (int i = 0; i < list.size() / 2; i++) {
                    View view = LayoutInflater.from(mActivity).inflate(R.layout.item_vp_v, null, false);
                    TextView tvContent1 = view.findViewById(R.id.tv_content1);
                    TextView tvContent2 = view.findViewById(R.id.tv_content2);
                    ImageView iv1 = view.findViewById(R.id.iv_rank1);
                    ImageView iv2 = view.findViewById(R.id.iv_rank2);
                    String rank1 = list.get(2 * i).getRank();
                    String rank2 = list.get(2 * i + 1).getRank();
                    if ("1".equals(rank1)) iv1.setImageResource(R.mipmap.icon_number_one);
                    else if("2".equals(rank1)) iv1.setImageResource(R.mipmap.icon_number_two);
                    else if("3".equals(rank1)) iv1.setImageResource(R.mipmap.icon_number_three);
                    else if("4".equals(rank1)) iv1.setImageResource(R.mipmap.icon_number_four);
                    else if("5".equals(rank1)) iv1.setImageResource(R.mipmap.icon_number_five);
                    if("1".equals(rank2)) iv2.setImageResource(R.mipmap.icon_number_one);
                    else if("2".equals(rank2)) iv2.setImageResource(R.mipmap.icon_number_two);
                    else if("3".equals(rank2)) iv2.setImageResource(R.mipmap.icon_number_three);
                    else if("4".equals(rank2)) iv2.setImageResource(R.mipmap.icon_number_four);
                    else if("5".equals(rank2)) iv2.setImageResource(R.mipmap.icon_number_five);
                    if(list.get(2*i).getMemberName()!=null) tvContent1.setText("恭喜"+list.get(2*i).getBranchAbbrName()+list.get(2*i).getMemberName()+"成交一单"+list.get(2*i).getProductAbbrName());
                    if(list.get(2*i+1).getMemberName()!=null) tvContent2.setText("恭喜"+list.get(2*i+1).getBranchAbbrName()+list.get(2*i+1).getMemberName()+"成交一单"+list.get(2*i+1).getProductAbbrName());
                    vp.addView(view);
                }
                vp.startFlipping();
            }
        }
    }

    //解析返回信息，构建列表
    public List<RealTimeOrderBean> getRealTimeOrder(JSONObject jsonObject){
        List<RealTimeOrderBean>  list = new ArrayList<RealTimeOrderBean>();
        try{
            JSONArray jarr = jsonObject.getJSONArray("data");
            /**循环list对象**/
            RealTimeOrderBean info = null;
            for (int i = 0; i < jarr.size(); i++) {
                JSONObject jsono = (JSONObject) jarr.get(i);
                /**取出list下的name的值 **/
                info = new RealTimeOrderBean();
                info.setContractId(getStringValue(jsono,"contractId"));
                info.setRank(getStringValue(jsono,"rank"));
                info.setMemberName(getStringValue(jsono,"memberName"));
                info.setBranchAbbrName(getStringValue(jsono,"branchAbbrName"));
                info.setAnnualPrem(getStringValue(jsono,"annualPrem"));
                info.setProductAbbrName(getStringValue(jsono,"productAbbrName"));
                info.setPaymentPeriod(getStringValue(jsono,"paymentPeriod"));
                info.setPaymentPeriodType(getStringValue(jsono,"paymentPeriodType"));
                info.setPaymentPeriodDesc(getStringValue(jsono,"paymentPeriodDesc"));
                info.setPreviewOrderImg(getStringValue(jsono,"previewOrderImg"));
                list.add(info);
            }
        } catch (Exception e) {
            Logger.i(TAG, e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public org.json.JSONObject getJSON(String sb) throws JSONException {
        return new org.json.JSONObject(sb);
    }

    public String getStringValue(JSONObject json, String key){
        String ret = "";
        try{
            ret = json.getString(key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ret;
    }

    private void fillLabel(LinearLayout ll_label,String label){
        ll_label.removeAllViews();
        String[] names = label.split(",");
        int size = names.length; // 添加Button的个数
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT); // 每行的水平LinearLayout
        layoutParams.setMargins(0, 20, 10, 3);
        ArrayList<TextView> childTvs = new ArrayList<TextView>();
        int totoalTvs = 0;
        for(int i = 0; i < size; i++){
            String item = names[i];
            LinearLayout.LayoutParams itemParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int length= item.length();
            if(length < 4){
                //itemParams.weight = 1;
                totoalTvs++;
            }else if(length < 8){
                //itemParams.weight = 2;
                totoalTvs+=2;
            }else{
                //itemParams.weight = 3;
                totoalTvs+=3;
            }
            //itemParams.width = 0;
            itemParams.setMargins(5, 3, 5, 3);
            TextView childTv = (TextView) LayoutInflater.from(mActivity).inflate(R.layout.item_label, null);
            childTv.setText(item);
            childTv.setLayoutParams(itemParams);
            childTvs.add(childTv);
            if(totoalTvs >= 2){
                LinearLayout  horizLL = new LinearLayout(mActivity);
                horizLL.setOrientation(LinearLayout.HORIZONTAL);
                horizLL.setLayoutParams(layoutParams);
                for(TextView addTv:childTvs){
                    horizLL.addView(addTv);
                }
                ll_label.addView(horizLL);
                childTvs.clear();
                totoalTvs = 0;
            }
        }
        //最后一行添加一下
        if(!childTvs.isEmpty()){
            LinearLayout  horizLL = new LinearLayout(mActivity);
            horizLL.setOrientation(LinearLayout.HORIZONTAL);
            horizLL.setLayoutParams(layoutParams);
            for(TextView addTv:childTvs){
                horizLL.addView(addTv);
            }
            ll_label.addView(horizLL);
            childTvs.clear();
            totoalTvs = 0;
        }
    }

    /**
     * 刷新未读消息
     */
    public void fresh_message(){
        if(checkLogin()){
            RequestManager
            .mRetrofitManager
            .createRequest(RetrofitRequestInterface.class)
            .getUnReadMessage()
            .enqueue(new RetrofitCallBack() {
                @Override
                public void onSuccess(String response) {
                    Logger.i("red---"+ response);
                    JSONObject jsonObject = JSON.parseObject(response);
                    String rCode = jsonObject.getString("code");
                    String rMsg  = jsonObject.getString("msg");
                    if("1000".equals(rCode)) {
                        init(jsonObject);
                    }else{
                        Toast.makeText(getActivity(),rMsg,Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onError(Throwable t) {
                    Logger.i(TAG, t.getMessage());
                    Toast.makeText(getActivity(),"获取未读消息失败"+t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    //初始界面未读标识
    public void init(JSONObject jsonObject){
        if(jsonObject!=null) {
            JSONArray jarr = jsonObject.getJSONArray("data");
            int msgCount = 0;
            int productCount = 0;
            for (int i = 0; i < jarr.size(); i++) {
                JSONObject jsono = (JSONObject) jarr.get(i);
                String msgType = getStringValue(jsono, "messageType");
                try {
                    if ("01".equals(msgType) || "03".equals(msgType))
                        msgCount = msgCount + Integer.valueOf(getStringValue(jsono, "unreadCount"));
                    if ("04".equals(msgType))
                        productCount = productCount + Integer.valueOf(getStringValue(jsono, "unreadCount"));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            if(msgCount > 0){
                iv_tip.setVisibility(View.VISIBLE);
            }else{
                iv_tip.setVisibility(View.GONE);
            }
            if(productCount > 0){
                iv_producttip.setVisibility(View.VISIBLE);
            }else{
                iv_producttip.setVisibility(View.GONE);
            }
        }else{
            iv_tip.setVisibility(View.GONE);
            iv_producttip.setVisibility(View.GONE);
        }
    }

    /**
     * 校验当前用户是否登录
     */
    public boolean checkLogin(){
        if(!"".equals(SPUtil.get("phone",""))){
            return true;
        }else{
            return false;
        }
    }

    private PopupWindow mPopupWindow;

    private void showPopupWindow() {
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        } else {
            View view = mInflater.inflate(R.layout.pop_private, null);
            TextView tvProtocol = view.findViewById(R.id.tv_protocol);
            TextView tvPolicy = view.findViewById(R.id.tv_policy);
            TextView tvCancel = view.findViewById(R.id.tv_cancel);
            TextView tvConfirm = view.findViewById(R.id.tv_confirm);

            tvProtocol.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toProtocol();
                }
            });
            tvPolicy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toPolicy();

                }
            });
            tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPopupWindow.dismiss();
                    ActivityUtil.getInstance().exitApp();
                }
            });
            tvConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPopupWindow.dismiss();
                   SPUtil.putValue(Constant.USER_INFO, Constant.FIRST_OPEN,true);

                }
            });


            mPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mPopupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
        }

    }

    private void toProtocol(){
        Intent intent = new Intent(mActivity,WebActivity.class);
        intent.putExtra("purl", Constants.protocolUrl);
        startActivity(intent);
    }

    private void toPolicy(){
        Intent intent = new Intent(mActivity,WebActivity.class);
        intent.putExtra("purl", Constants.privateUrl);
        startActivity(intent);
    }
    
    public ProductListBean.DataDTO.ListDTO copyProperties(ProductListBean.DataDTO.ListDTO bean1,ProductHotBean.ListDTO bean2){
        bean1.setProductName(bean2.getProductAbbrName());
        bean1.setProductAbbrDescription(bean2.getProductAbbrDescription());
        bean1.setIsHot(bean2.getIsHot());
        bean1.setIsOnline(bean2.getIsOnline());
        bean1.setProductId(bean2.getProductId());
        bean1.setProductUrl(bean2.getProductUrl());
        bean1.setLogoUrl(bean2.getLogoUrl());
        bean1.setPartnerUrl(bean2.getPartnerUrl());
        return bean1;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    public void refreshList(RefreshHomePageEvent refreshHomePageEvent) {
        getBillBoard();
        getHotProducts();
        fresh_message();
    }

}

