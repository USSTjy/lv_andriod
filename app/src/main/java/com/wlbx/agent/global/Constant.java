package com.wlbx.agent.global;

/**
 * Created by Muzik on 2019/3/23
 */
public class Constant {
    public static final String SP_NAME = "";

    public static final String SEARCH_HISTORTY = "search_history" ;
    public static final String USER_INFO = "USER_INFO";
    public static final String TOKEN = "TOKEN";



    public static final String LOGIN_STATE = "LOGIN_STATE";
    public static final String PHONE_PATTERN = "[1]{1}[3,4,5,6,7,8,9]{1}[0-9]{9}";
    public static final String PW_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,20}$";

    public static final String FIRST_OPEN = "FIRST_OPEN";

    public static  boolean INSTALL_LATER = true;




    /**
     * wx_id
     */
    public static final String WECHAT_APP_ID = "wx11971c1e18a7e9";
    /**
     * 商户号
     */
    public static final String WECHAT_MCH_ID = "134074071";
    /**
     * 密钥
     */
    public static final String WECHAT_API_PRIVATE_KEY = "jjssjjssjjssjjssssjjssjjss8888";


    //出单方式
    public static final String BUNDLE_KEY_METHOD = "key_method";
    //合作机构
    public static final String BUNDLE_KEY_AGENT = "key_agent";
    //产品类型
    public static final String BUNDLE_KEY_PRODUCT_TYPE = "key_type";
    //适用人群
    public static final String BUNDLE_KEY_PRODUCT_GROUP = "key_group";
    //搜索关键字
    public static final String BUNDLE_KEY_PRODUCT_NAME = "key_name";
    //判断指纹识别
    public static  boolean FINGER_PRINT = true;

    public static  String PRODUCT_NAME = "";

    public static  String PARTNER_ID = "";

    public static  String IS_ONLINE = "";

    public static  String PRODUCT_SUB_TYPE="";





}
