package com.wlbx.agent.global;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.wlbx.agent.base.BaseActivity;
import com.wlbx.agent.bean.AccountInfoBean;
//import com.wlbx.agent.ui.activity.LoginActivity;
import com.wlbx.agent.util.ActivityUtil;
import com.wlbx.agent.util.SPUtil;
public  class AccountManager {
    public static AccountInfoBean sUserBean;

    public static void loginOut(Context context) {
        removeSP();
//        context.startActivity(new Intent(context, LoginActivity.class));
//        ActivityUtil.getInstance().finishAllActivityExcept(LoginActivity.class);
        if (context instanceof Activity) {
            ((Activity) context).finish();
        }
        BaseActivity.removeQuitToLoginListern();


    }

    public static void removeSP() {
        SPUtil.removeValue(Constant.USER_INFO,"agent");
//        SPUtil.removeValue(Constant.USER_INFO,Constant.BUNDLE_KEY_PRODUCT_NAME);
        SPUtil.remove("Public-Key");
        SPUtil.remove("Access-Token");
        SPUtil.remove("phone");
        SPUtil.remove(Constant.LOGIN_STATE);
//        SPUtil.clearAll();
        AccountManager.sUserBean = null;
    }



    public static  boolean hasUserLogin(){
        return (Boolean) SPUtil.get(Constant.LOGIN_STATE, false);
    }
    public static void setUserLogin(boolean isLogin){
        SPUtil.put(Constant.LOGIN_STATE, isLogin);
    }

//    public static UserBean getCurrentUser(){
//        String json_user = (String) SPUtil.get(ConstantCfg.SP_USER, "");
//        return new Gson().fromJson(json_user, UserBean.class);
//    }
//
//    public static void setCurrentUser(UserBean userBean){
//        String json_user = new Gson().toJson(userBean, UserBean.class);
//        SPUtils.put(ConstantCfg.SP_USER, json_user);
//    }
//
//
//    public static void currentUserLogout(){
//        SPUtils.put(ConstantCfg.SP_USER, "");
//        SPUtils.put(ConstantCfg.SP_TOKEN, "");
//        SPUtils.put(ConstantCfg.SP_HAS_UER_LOGIN, false);
//    }
//
//    public static String getToken(){
//        return (String) SPUtils.get(ConstantCfg.SP_TOKEN, "");
//    }


}
