package com.wlbx.agent.global;

import com.wlbx.agent.BuildConfig;

public class Common {
	public final static String AOSP_FORMAT=
			"<soapenv:Envelope xmlns:soapenv=\"https://schemas.xmlsoap.org/soap/envelope/\" xmlns:cxf=\"https://cxf.spring.core.sinosoft.com/\">\n"
			+ "<soapenv:Header/>\n"
			+ "<soapenv:Body>\n"
			+ "<cxf:%s>\n"
			+ "<!--Optional:-->\n"
			+ "<requestParam>%s</requestParam>\n"
			+ "</cxf:%s>\n"
			+ "</soapenv:Body>\n"
			+ "</soapenv:Envelope>";

	public final static String SP_FILE="wlbx.config";
	public final static String SP_FIELD_DOWNLOADID="appdownloadid";
	public final static String SP_FIELD_VERSIONNAME="versionName";
	public static final String SP_FIELD_UPDATETIME="updatetime";

	public final static String SP_FILELE_AGENTMOBILE="agentMobile";
	public final static String SP_FILELE_AGENTNAME="agentName";

	public final static String SERVICE_NS = "https://cxf.spring.core.sinosoft.com/";


	//法律协议专用
	//	public final static String URL="https://183.129.194.13";		      //生产IP
	//  public final static String URL="https://ins.zeshukeji.com"; 	      //生产域名
	//  public final static String URL="https://wanlibaoxian.com";			  //生产域名
	//	public final static String URL="https://ceshi.zeshukeji.com/wlbx";    //测试https域名
	//	public final static String URL="https://ceshi.zeshukeji.com/wlbx02";  //测试2
	//	public final static String URL="http://124.160.93.187:8080/wlbx";	  //uat测试外网
	//	public final static String URL="https://10.68.60.235:8080/wlbx";
	//	public final static String URL="https://10.67.30.76:8080/wlbx";		  //uat测试内网
	//	public final static String URL="http://10.68.60.234:8080/wlbx";       //特殊本地ip



 	public final static String URL="https://pms.wanlibaoxian.com";//生产域名
//	public final static String URL="https://ceshi.wanlibaoxian.com/wlbx";//测试域名
//	public final static String URL="https://ceshi.wanlibaoxian.com/wlbx01";//测试域名
//	public final static String URL="https://ceshi.wanlibaoxian.com/wlbx02";//测试域名
	public final static String GUIDE_URL = URL + "/riskProductApp/greenHandGuideUrl"; //新手指引
	public final static String MAIN_SHARE_URL = URL + "/riskProductApp/riskRequiremengInfoUrl?agentId="; //首页分享页面，可以传用户id
	public final static String PRIVACY_POLICY_URL = URL + "/riskProductApp/privacyInformPage";//隐私政策

	public final static String SERVICE_URL =URL+"/services/riskAppServer?wsdl";
	public final static String projectName = "wlbx";

	public static String path = "wlbx.config";
	public static String canSee = "true";
	public static String mobile = "";
	public static String ifcomit = "";
	public static String agentId = "";
	public static String isGroupLeader = "";
	public static String md5Key = "yY8FhoBEvWzFhms6oBBlDHkoLQMxJlXu";//生产
	public static String soapProperty = "requestParam";
	public static String sharePageUrl="";
    public static String clipTextCache="";
	public static int requestPermissionGenerator=1;
	//是否输出本app的log信息
	public final static boolean DEBUG = BuildConfig.DEBUG;
	//微信分享的appid
	//public final static String APP_ID="wxd57a7e269383a623";
	//public final static String APP_ID="wxd2aa6400a7252c55";
	public final static String APP_ID="wx7f9b96af50978b1d";
}
