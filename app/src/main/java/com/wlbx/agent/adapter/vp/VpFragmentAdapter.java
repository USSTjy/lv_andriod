package com.wlbx.agent.adapter.vp;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class VpFragmentAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragmentList;

    public VpFragmentAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.mFragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);

    }

    @Override
    public int getCount() {
        return  mFragmentList == null ? 0 : mFragmentList.size();
    }

}
