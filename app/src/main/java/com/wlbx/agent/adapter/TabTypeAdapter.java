package com.wlbx.agent.adapter;

import android.annotation.SuppressLint;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.wlbx.agent.R;
import com.wlbx.agent.bean.BillboardListBean;
import com.wlbx.agent.bean.ProductSubType;

import org.jetbrains.annotations.NotNull;

/**
 * 横向标签
 */
public class TabTypeAdapter extends BaseQuickAdapter<ProductSubType.DataDTO, BaseViewHolder>  {

    public TabTypeAdapter() {
        super(R.layout.item_type);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, ProductSubType.DataDTO bean) {
        holder.setText(R.id.tv_type,bean.getProductSubTypeName());
        boolean selected = bean.isSelected();
        if (selected) {
            holder.setBackgroundResource(R.id.tv_type,R.drawable.selector_checked_type);
            holder.setTextColor(R.id.tv_type,getContext().getColor(R.color.white));
        } else {
            holder.setBackgroundResource(R.id.tv_type,R.drawable.selector_default_type);
            holder.setTextColor(R.id.tv_type,getContext().getColor(R.color.software_textColor_unselected));
        }
    }
}

