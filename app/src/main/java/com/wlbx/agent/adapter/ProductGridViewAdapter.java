package com.wlbx.agent.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.wlbx.agent.R;
import com.wlbx.agent.bean.ProductHotBean;
import com.wlbx.agent.bean.ProductListBean;
import java.util.List;

public class ProductGridViewAdapter extends BaseAdapter {
    //声明引用
    private Context mContext;   //这个Context类型的变量用于第三方图片加载时用到
    private LayoutInflater mlayoutInflater;
    private List<ProductHotBean.ListDTO> mList;
    //创建一个构造函数
    public ProductGridViewAdapter(Context context, List<ProductHotBean.ListDTO> list){
        this.mContext=context;
        this.mList = list;
        //利用LayoutInflate把控件所在的布局文件加载到当前类中
        mlayoutInflater=LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return mList.size(); //GridView的数目
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    //写一个静态的class,把layout_grid_item的控件转移过来使用
    static class ViewHolder{
        public TextView tv_name;
        public ImageView iv_bk;
        public ImageView iv_g;
        public TextView tv_desc;
        public TextView tv_fee;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProductHotBean.ListDTO fe = mList.get(position);
        ViewHolder holder = null;
        if(convertView == null){
            //把控件所在的布局文件加载到当前类中
            convertView = mlayoutInflater.inflate(R.layout.item_gv_product,null);
            //生成一个ViewHolder的对象
            holder = new ViewHolder();
            //获取控件对象
            holder.tv_name=convertView.findViewById(R.id.tv_name);
            holder.iv_bk=convertView.findViewById(R.id.iv_bk);
            holder.iv_g = convertView.findViewById(R.id.iv_g);
            holder.tv_desc=convertView.findViewById(R.id.tv_desc);
            holder.tv_fee=convertView.findViewById(R.id.tv_fee);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        //修改空间属性
        holder.tv_name.setText(fe.getProductName());
        holder.tv_desc.setText(fe.getProductAbbrDescription());
        holder.tv_fee.setText(fe.getStartPrice());
        if(position==0) holder.iv_g.setImageResource(R.mipmap.top_g1);
        else holder.iv_g.setImageResource(R.mipmap.top_g2);
        Glide.with(mContext).load(fe.getBackgroundUrl()).into(holder.iv_bk);
        return convertView;
    }
}
