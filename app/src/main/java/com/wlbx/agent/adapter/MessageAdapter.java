package com.wlbx.agent.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.wlbx.agent.R;
import com.wlbx.agent.bean.MessageListBean;
import com.wlbx.agent.ui.activity.WebActivity;
import com.wlbx.agent.util.Constants;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 消息列表
 */
public class MessageAdapter extends BaseQuickAdapter<MessageListBean.DataDTO.ListDTO, BaseViewHolder>  implements LoadMoreModule{
    private Context mContext;

    public MessageAdapter(Context mContext) {
        super(R.layout.item_message);
        this.mContext = mContext;
    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, MessageListBean.DataDTO.ListDTO bean) {
        holder.setText(R.id.tv_messagetitle, bean.getMessageTitle())
              .setText(R.id.tv_messagecontent,bean.getMessageContent())
              .setText(R.id.tv_time,bean.getSendTime());
        ImageView tip = holder.getView(R.id.iv_tips);
        if("Y".equals(bean.getReadFlag())) tip.setVisibility(View.GONE); else tip.setVisibility(View.VISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { toDetail(bean);}
        });
    }

    public void toDetail(MessageListBean.DataDTO.ListDTO bean){
        Intent intent = new Intent(mContext, WebActivity.class);
        Date dateTime = new Date();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(dateTime);
        intent.putExtra("purl", Constants.sysMesUrl+"?id="+bean.getId()+"&sendData="+"&sendTime="+bean.getSendTime());
        mContext.startActivity(intent);
    }
}

