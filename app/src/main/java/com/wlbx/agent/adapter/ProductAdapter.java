package com.wlbx.agent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.wlbx.agent.R;
import com.wlbx.agent.bean.InOnlineBean;
import com.wlbx.agent.bean.ProductListBean;
import androidx.appcompat.widget.AppCompatImageView;

import org.jetbrains.annotations.NotNull;
import com.bumptech.glide.Glide;
import com.zhy.http.okhttp.utils.L;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品列表
 */
public class ProductAdapter extends BaseQuickAdapter<ProductListBean.DataDTO.ListDTO, BaseViewHolder>  implements LoadMoreModule{

    private Context mContext;
    private LayoutInflater mInflater;

    public ProductAdapter(Context context) {
        super(R.layout.item_product);
        mInflater = LayoutInflater.from(context);

        addChildClickViewIds(R.id.iv_logo_url);
    }





    @Override
    protected void convert(@NotNull BaseViewHolder holder, ProductListBean.DataDTO.ListDTO bean) {
//        holder.setVisible(R.id.tv_start_price,true);
        holder.setText(R.id.tv_product_name, bean.getProductName())
                .setText(R.id.tv_product_abbr_description,bean.getProductAbbrDescription())
//                .setText(R.id.tv_custom_label,TextUtils.isEmpty(bean.getCustomLabel())?"":bean.getCustomLabel().replaceAll(","," "))
                .setGone(R.id.tv_start_price, TextUtils.isEmpty(bean.getStartPrice())? true:false)
                .setText(R.id.tv_start_price, TextUtils.isEmpty(bean.getStartPrice())? "":bean.getStartPrice());

        Glide.with(holder.itemView).load(bean.getLogoUrl()).into((AppCompatImageView) holder.getView(R.id.iv_logo_url));
        holder.setText(R.id.tv_online,bean.getIsOnline().equals("Y")?"线上":"线下");
        holder.setBackgroundResource(R.id.tv_online,bean.getIsOnline().equals("Y")?R.drawable.shape_online_bg:R.drawable.shape_offline_bg);
        holder.setTextColor(R.id.tv_online,bean.getIsOnline().equals("Y")? Color.parseColor("#F68657") :Color.parseColor("#409eff"));
        if (!TextUtils.isEmpty(bean.getIsHot())){
            holder.setVisible(R.id.tv_hot,bean.getIsHot().equals("Y")?true:false);
        }
        if (!TextUtils.isEmpty(bean.getCustomLabel())){
        TagFlowLayout mFlowLayout=holder.getView(R.id.tfl_label);
        mFlowLayout.setAdapter(new TagAdapter<String>(bean.getCustomLabel().split(","))
        {
            @Override
            public View getView(FlowLayout parent, int position, String s)
            {
                TextView tv = (TextView) mInflater.inflate(R.layout.item_custom_label,
                        mFlowLayout, false);
                tv.setText(s);
                return tv;
            }
        });
        }
    }
}

