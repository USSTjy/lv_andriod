package com.wlbx.agent.adapter;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.wlbx.agent.R;
import com.wlbx.agent.bean.ProductListBean;
import androidx.appcompat.widget.AppCompatImageView;

import org.jetbrains.annotations.NotNull;
import com.bumptech.glide.Glide;
import com.wlbx.agent.bean.ProductNewsBean;

public class ProductNewsAdapter  extends BaseQuickAdapter<ProductNewsBean.DataDTO.ListDTO, BaseViewHolder> implements LoadMoreModule {

    public ProductNewsAdapter() {
//        addChildClickViewIds(R.id.iv_logo_url);
        super(R.layout.item_product_news);

    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, ProductNewsBean.DataDTO.ListDTO bean) {
        holder.setText(R.id.tv_message_title, bean.getProductName())
                .setText(R.id.tv_send_time,bean.getSendTime());
        Glide.with(holder.itemView).load(bean.getMessagePicUrl()).into((AppCompatImageView) holder.getView(R.id.iv_message_pic_url));
        holder.setVisible(R.id.tv_read_flag,bean.getReadFlag().equals("Y")?false:true);
    }
}