package com.wlbx.agent.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.module.LoadMoreModule;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.wlbx.agent.R;
import com.wlbx.agent.bean.BillboardListBean;
import com.wlbx.agent.bean.ProductListBean;

import org.jetbrains.annotations.NotNull;

/**
 * 榜单明细
 */
public class BillboardListAdapter extends BaseQuickAdapter<BillboardListBean.DataDTO.ListDTO, BaseViewHolder>  implements LoadMoreModule{

    public BillboardListAdapter() {
        super(R.layout.item_billboard);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder holder, BillboardListBean.DataDTO.ListDTO bean) {

        holder.setText(R.id.tv,"恭喜"+ bean.getBranchAbbrName()+bean.getMemberName()+"成交一单"+bean.getProductAbbrName()+" "+Math.round(bean.getAnnualPrem())+"元"+bean.getPaymentPeriodDesc());

        if (bean.getRank()==1){
            holder.setVisible(R.id.iv_icon_number,true);
            holder.setVisible(R.id.tv_icon_number,false);

                    Glide.with(holder.itemView).load(R.mipmap.icon_number_one).into((ImageView) holder.getView(R.id.iv_icon_number));
        }else if (bean.getRank()==2){
            holder.setVisible(R.id.iv_icon_number,true);
            holder.setVisible(R.id.tv_icon_number,false);

            Glide.with(holder.itemView).load(R.mipmap.icon_number_two).into((ImageView) holder.getView(R.id.iv_icon_number));

        }else if (bean.getRank()==3){
            holder.setVisible(R.id.iv_icon_number,true);
            holder.setVisible(R.id.tv_icon_number,false);

            Glide.with(holder.itemView).load(R.mipmap.icon_number_three).into((ImageView) holder.getView(R.id.iv_icon_number));

        }else if (bean.getRank()==4){
            holder.setVisible(R.id.iv_icon_number,true);
            holder.setVisible(R.id.tv_icon_number,false);

            Glide.with(holder.itemView).load(R.mipmap.icon_number_four).into((ImageView) holder.getView(R.id.iv_icon_number));

        }else if (bean.getRank()==5){
            holder.setVisible(R.id.iv_icon_number,true);
            holder.setVisible(R.id.tv_icon_number,false);

            Glide.with(holder.itemView).load(R.mipmap.icon_number_five).into((ImageView) holder.getView(R.id.iv_icon_number));
        }else {
            holder.setVisible(R.id.iv_icon_number,false);
            holder.setVisible(R.id.tv_icon_number,true);

            holder.setText(R.id.tv_icon_number,bean.getRank()+"");
        }

    }
}

