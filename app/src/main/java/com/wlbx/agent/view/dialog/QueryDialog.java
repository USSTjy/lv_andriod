package com.wlbx.agent.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.wlbx.agent.R;
import com.wlbx.agent.global.Constant;
import com.wlbx.agent.util.SPUtil;


public class QueryDialog extends Dialog implements View.OnClickListener {
    private  DialogGoback mDialogGoback;
    private  String title;
    private  String contexts;
    private  Context context;
    private  String forceUpdate;

    public QueryDialog(@NonNull Context context, String title, String contexts, String forceUpdate, DialogGoback mDialogGoback) {
        super(context);
        this.context = context;
        this.title  = title;
        this.contexts = contexts;
        this.mDialogGoback = mDialogGoback;
        this.forceUpdate = forceUpdate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LinearLayout.inflate(this.context, R.layout.query_dialog,null));
        initView();
    }

    private void initView() {
        TextView query_title = findViewById(R.id.query_title);
//        TextView query_context1 = findViewById(R.id.query_context1);
//        TextView query_context2 = findViewById(R.id.query_context2);
        TextView query_no_query = findViewById(R.id.query_no_query);//暂不更新
        TextView query_update = findViewById(R.id.query_update);//更新
        TextView mQueryDelete = findViewById(R.id.query_delete);//退出app


        query_title.setText("发现新版本，版本号："+title);
//        query_title.setText("发现新版本");

//        if(contexts != null){
//            if(contexts.contains("\r\n")){
//                String[] strings = contexts.split("\\r\\n");
//                query_context1.setText(strings[0]);
//                query_context2.setText(strings[1]);
//            }else{
//                query_context1.setText(contexts);
//                query_context2.setVisibility(View.GONE);
//            }
//        }

        if(forceUpdate.equals("Y")){
            //强制更新
            mQueryDelete.setVisibility(View.VISIBLE);
            query_no_query.setVisibility(View.GONE);
        }else{
            //非强制更新
            mQueryDelete.setVisibility(View.GONE);
            query_no_query.setVisibility(View.VISIBLE);

        }
        mQueryDelete.setOnClickListener(this);
        query_no_query.setOnClickListener(this);
        query_update.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.query_delete:
                System.exit(0);

                break;
            case R.id.query_no_query:
                dismiss();
                break;
            case R.id.query_update:
                mDialogGoback.query();

                break;
        }
    }

    public interface DialogGoback{
        void query();
    }
}
