package com.wlbx.agent.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import com.wlbx.agent.R;

/*微信、短信分享底部弹框*/
public class ShareDialog extends Dialog {

    private Context mContext;
    private onShare monShare;
    private LinearLayout layout_weixin;
    private LinearLayout layout_friend;

    private Button bt_cancel;

    private String shareMsg = "我在使用“保险E家”，掌上保险计划 书，寿险本地出单，推广费高达80%，可通过s%迅速加入，强烈推荐，快来试试吧!";

    private String recomType = "";

    public ShareDialog(Context context, String recomType) {
        super(context);
        mContext = context;
        this.recomType = recomType;
    }

    public ShareDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_share);
        getWindow().setGravity(Gravity.BOTTOM);
        getWindow().setWindowAnimations(R.style.share_pop_anim);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        layout_weixin = (LinearLayout) findViewById(R.id.layout_sharedialog_weixin);
        layout_friend = (LinearLayout) findViewById(R.id.layout_sharedialog_frient);
        bt_cancel = (Button) findViewById(R.id.bt_sharedialog_cancel);
        layout_weixin.setOnClickListener(onClickListener);
        layout_friend.setOnClickListener(onClickListener);
        bt_cancel.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.layout_sharedialog_weixin:
                    if (monShare != null) {
                        monShare.onShareToWeixin();
                    }
                    break;
                case R.id.layout_sharedialog_frient:
                    if (monShare != null) {
                        monShare.onShareToFriends();
                    }
                    break;
                case R.id.bt_sharedialog_cancel:
                    break;
                default:
                    break;
            }
            ShareDialog.this.dismiss();
        }
    };

    public void setMonShare(onShare monShare) {
        this.monShare = monShare;
    }


    public interface onShare {
        void onShareToWeixin();
        void onShareToFriends();
    }
}
