package com.wlbx.agent.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 解决ScrollView嵌套ListView，ListView显示不全的问题
 * 注意：ScrollView嵌套ListView时，如果ListView很长超出了屏幕的高度，
 * 那么ScrollView会自动滚动到ListView的顶部，
 * 但是我们需要默认在整体页面顶部，所以在初始化的时候就让ScrollView获得焦点，滚动条自然就显示到顶部了。
 * scrollView.setFocusable(true);
 * scrollView.setFocusableInTouchMode(true);
 * scrollView.requestFocus();
 */
public class GridViewForScrollView extends GridView {


    public GridViewForScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridViewForScrollView(Context context) {
        super(context);
    }

    public GridViewForScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
