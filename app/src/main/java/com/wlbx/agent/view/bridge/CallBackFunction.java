package com.wlbx.agent.view.bridge;

public interface CallBackFunction {
	
	public void onCallBack(String data);

}
