package com.wlbx.agent.view.bridge;

public interface BridgeHandler {
	
	void handler(String data, com.wlbx.agent.view.bridge.CallBackFunction function);

}
