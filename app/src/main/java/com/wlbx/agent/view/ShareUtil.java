package com.wlbx.agent.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.fastlib.utils.N;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wlbx.agent.global.Common;
import java.io.ByteArrayOutputStream;

public class ShareUtil {
    private ShareUtil() {
    }

    /**
     * 分享图片到微信
     * @param context 上下文
     * @param bitmap 图片
     * @param shareToFriend true分享
     */
    public static void shareImageToWechat(Context context, Bitmap bitmap, boolean shareToFriend){
        Bitmap shareBitmap=bitmap.copy(Bitmap.Config.ARGB_4444,true);
        IWXAPI api = WXAPIFactory.createWXAPI(context, Common.APP_ID, true);
        api.registerApp(Common.APP_ID);
        float multiple=1;
        while(shareBitmap.getByteCount()>3000000){
            if(multiple<=0.1f)
                multiple-=0.01f;
            else multiple-=0.1;
            shareBitmap.recycle();
            shareBitmap=Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*multiple),(int)(bitmap.getHeight()*multiple),false);
        }
        WXImageObject imgObj=new WXImageObject(shareBitmap);
        WXMediaMessage msg=new WXMediaMessage();
        msg.mediaObject=imgObj;

        Bitmap thumbBitmap=Bitmap.createScaledBitmap(shareBitmap,80,80,true);
        msg.thumbData=bmpToByteArray(thumbBitmap,true);

        SendMessageToWX.Req req=new SendMessageToWX.Req();
        req.transaction=String.valueOf(System.currentTimeMillis());
        req.message=msg;
        req.scene=shareToFriend?SendMessageToWX.Req.WXSceneSession:SendMessageToWX.Req.WXSceneTimeline;
        api.sendReq(req);
        bitmap.recycle();
    }

    public static void shareToWeiXin(Context context, String title, String description, String url, Bitmap icon) {
        IWXAPI api = WXAPIFactory.createWXAPI(context, Common.APP_ID, true);
        api.registerApp(Common.APP_ID);
        try {
            Log.i("shareToWeiXin:", "title:" + title + " description:" + description + " url:" + url);
            if (!api.isWXAppInstalled()){
                N.showShort(context, "还没有安装微信");
                return;
            }
            WXWebpageObject webObj = new WXWebpageObject();
            webObj.webpageUrl = url;

            WXMediaMessage message = new WXMediaMessage(webObj);
            message.title = title;
            message.description = description;
            if (icon != null) {
                Bitmap bitmap = icon.copy(icon.getConfig(), false);
                Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, 80, 80, true);
                message.thumbData = bmpToByteArray(thumbBmp, true);
            }

            SendMessageToWX.Req req = new SendMessageToWX.Req();
            req.transaction = String.valueOf(System.currentTimeMillis());
            req.message = message;
            req.scene = SendMessageToWX.Req.WXSceneSession;

            api.sendReq(req);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 微信朋友圈分享
     * @param context
     * @param title
     * @param description
     * @param url
     * @param icon
     */
    public static void shareToFriends(Context context, String title, String description, String url, Bitmap icon) {
        IWXAPI api = WXAPIFactory.createWXAPI(context, Common.APP_ID, true);
        api.registerApp(Common.APP_ID);
        try {
            Log.i("shareToWeiXin:", "title:" + title + " description:" + description + " url:" + url);
            if (!api.isWXAppInstalled()) {
                N.showShort(context, "还没有安装微信");
                return;
            }

            WXWebpageObject webObj = new WXWebpageObject();
            webObj.webpageUrl = url;

            WXMediaMessage message = new WXMediaMessage(webObj);
            message.title = title;
            message.description = description;
            if (icon != null) {
                Bitmap bitmap = icon.copy(icon.getConfig(), false);
                Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, 80, 80, true);
                message.thumbData = bmpToByteArray(thumbBmp, true);
            }

            SendMessageToWX.Req req = new SendMessageToWX.Req();
            req.transaction = String.valueOf(System.currentTimeMillis());
            req.message = message;
            req.scene = SendMessageToWX.Req.WXSceneTimeline;
            api.sendReq(req);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        byte[] result;
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
            if (needRecycle) {
                bmp.recycle();
            }
            result = output.toByteArray();
            try {
                output.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }
}