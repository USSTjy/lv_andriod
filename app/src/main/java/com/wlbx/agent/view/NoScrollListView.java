package com.wlbx.agent.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * @author jiangchao
 * @Module.Name
 * @Create.Date 2018/5/12 下午12:37
 * @Update.By jiangchao
 * @Update.Content
 * @Update.Date 2018/5/12 下午12:37
 * @see
 */


public class NoScrollListView extends ListView {
    public NoScrollListView(Context context) {
        super(context);
    }

    public NoScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoScrollListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * 设置listView不可滑动
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
