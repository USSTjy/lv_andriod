package com.wlbx.agent.event;

public class HiddenTabEvent {
   private boolean isHidden ;

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }
}
