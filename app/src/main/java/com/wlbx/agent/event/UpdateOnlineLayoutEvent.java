package com.wlbx.agent.event;

public class UpdateOnlineLayoutEvent {
    private String isOnline;

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }
}
