/*
 * @Author       : JiangChao
 * @Date         : 2021-10-11 19:48:50
 * @LastEditors  : JiangChao
 * @LastEditTime : 2021-10-11 19:49:05
 * @Description  : 
 */
package com.wlbx.agent.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import com.orhanobut.logger.Logger;
import com.wlbx.agent.BuildConfig;
import com.wlbx.agent.global.App;
import com.wlbx.agent.net.IQuitToLogin;
import com.wlbx.agent.util.ActivityUtil;
import com.wlbx.agent.util.statusbar.QMUIStatusBarHelper;
import com.wlbx.agent.view.DialogWaiting;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseWebActivity extends AppCompatActivity implements BaseView {

    private static final String TAG = "BaseWebActivity";
    protected Context mContext;
    protected Activity mActivity;
    protected App mApp;
    protected ActivityUtil mActivityUtil;
    protected LayoutInflater mInflater;
    private Unbinder mUnBinder;
    private static IQuitToLogin quitToLoginListener;

    /**
     * 获取布局Id
     *
     * @return
     */
    protected abstract int getContentViewId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        mActivity = this;
        mApp = App.getInstance();
        mInflater = LayoutInflater.from(this);
        mActivityUtil = ActivityUtil.getInstance();
        mActivityUtil.addActivity(this);

        beforeSetContentView();
        setContentView(getContentViewId());
        QMUIStatusBarHelper.translucent(this);
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        mUnBinder = ButterKnife.bind(this);
        initData();
        initEvent();
    }


    /**
     * setContentView之前调用
     */
    protected void beforeSetContentView() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // 设置没有标题栏
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnBinder!=null){
            mUnBinder.unbind();
        }
        mActivityUtil.removeActivity(this);
    }

    private boolean DEBUG = true;//是否输出调试log信息
    private DialogWaiting netWorkWaitingDialog;

    protected void setDebug(boolean debug) {
        DEBUG = debug;
    }

    protected void showWaitingDialog() {
        if (netWorkWaitingDialog == null) {
            netWorkWaitingDialog = new DialogWaiting(this);
        } else {
            debugE("netWorkWaitingDialog正在显示");
        }
        netWorkWaitingDialog.show();
    }

    protected void dismissWaitingDialog() {
        if (netWorkWaitingDialog != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    netWorkWaitingDialog.dismiss();
                    netWorkWaitingDialog = null;
                }
            });
        } else {
            debugE("netWorkWaitingDialog不曾显示");
        }
    }

    protected void debugI(String... args) {
        if (DEBUG && BuildConfig.DEBUG) {
            StringBuffer buffer = new StringBuffer();
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            StackTraceElement tmp = trace[3];
            buffer.append('[').append(tmp.getLineNumber()).append(']');
            if (args != null) {
                for (String temp : args) {
                    buffer.append(temp);
                }
            } else {
                buffer.append("null");
            }
            Log.i(this.getClass().getName(), buffer.toString());
        }
    }

    protected void debugE(String... args) {
        if (BuildConfig.DEBUG) {
            StringBuffer buffer = new StringBuffer();
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            StackTraceElement tmp = trace[3];
            buffer.append('[').append(tmp.getLineNumber()).append(']');
            for (String temp : args) {
                buffer.append(temp);
            }
            Log.e(this.getClass().getName(), buffer.toString());
        }
    }

}

