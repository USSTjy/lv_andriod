package com.wlbx.agent.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wlbx.agent.global.App;
import com.wlbx.agent.util.SPUtil;
import com.wlbx.agent.util.ScreenUtils;
//import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements BaseView {


    protected Fragment mFragment;
    protected App mApp;
    protected Activity mActivity;
    protected LayoutInflater mInflater;
    protected ViewGroup mContainer;
    protected View mRootView;
    public Context context ;

    private Unbinder mUnbinder;

//    QMUITipDialog tipDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();

    }

    /**
     * 获取布局Id
     *
     * @return InflateViewId
     */
    protected abstract int getInflateViewId();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragment = this;
        mApp = App.getInstance();
        mInflater = inflater;
        mContainer = container;
        initInstanceState(savedInstanceState);
        beforeInflateView();
        context = getActivity();

        if (mRootView == null) {
            mRootView = inflater.inflate(getInflateViewId(), container, false);
            mUnbinder = ButterKnife.bind(this, mRootView);
            initData();
            initEvent();
        }
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }
        return mRootView;
    }

    /**
     * 初始化保存的数据
     */
    protected void initInstanceState(Bundle savedInstanceState) {

    }

    /**
     * inflateView之前调用
     */
    protected void beforeInflateView() {
    }


    /**
     * 通过id初始化View
     *
     * @param viewId viewId
     * @param <T>    T
     * @return T
     */
    protected <T extends View> T byId(int viewId) {
        return (T) mRootView.findViewById(viewId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbinder!=null){
            mUnbinder.unbind();
        }
    }


    public void showTipDialog() {
//        if (tipDialog == null) {
//            tipDialog = new QMUITipDialog.Builder(mContext)
//                    .setIconType(iconType)
//                    .setTipWord(msg)
//                    .create();
//        }
//        tipDialog.show();
//        if (tipDialog == null) {
//            tipDialog = new QMUITipDialog.Builder(getContext())
//                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
//                    .setTipWord("正在请求服务器，请稍后...")
//                    .create();
//            tipDialog.show();
//        }
    }


    public void hideTipDialog() {
//        if (tipDialog != null) {
//            tipDialog.dismiss();
//        }
    }



    /**
     * 校验当前用户是否登录
     */
    public boolean checkLogin(){
        if(!"".equals(SPUtil.get("phone",""))){
            return true;
        }else{
            return false;
        }
    }
}

