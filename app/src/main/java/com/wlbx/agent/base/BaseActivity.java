package com.wlbx.agent.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

//import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;
import com.wlbx.agent.R;
import com.wlbx.agent.global.App;
import com.wlbx.agent.net.IQuitToLogin;
import com.wlbx.agent.util.ActivityUtil;
import com.wlbx.agent.util.ScreenUtils;
import com.wlbx.agent.util.statusbar.QMUIStatusBarHelper;
import com.wlbx.agent.view.dialog.CustomDialogSureCancel;
//import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    private static final String TAG = "BaseActivity";
    protected Context mContext;
    protected Activity mActivity;
    protected App mApp;
    protected ActivityUtil mActivityUtil;
    protected LayoutInflater mInflater;
    private Unbinder mUnBinder;
//    QMUITipDialog tipDialog;
    private static IQuitToLogin quitToLoginListener;

    /**
     * 获取布局Id
     *
     * @return
     */
    protected abstract int getContentViewId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        mActivity = this;
        mApp = App.getInstance();
        mInflater = LayoutInflater.from(this);
        mActivityUtil = ActivityUtil.getInstance();
        mActivityUtil.addActivity(this);

        beforeSetContentView();
        setContentView(getContentViewId());
//        QMUIStatusBarHelper.translucent(this);
//        QMUIStatusBarHelper.setStatusBarDarkMode(this);
        ScreenUtils.setStatusBarTransparentAndWordsGray(getWindow());

        mUnBinder = ButterKnife.bind(this);
        initData();
        initEvent();
        registerReceivers();
    }


    /**
     * setContentView之前调用
     */
    protected void beforeSetContentView() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // 设置没有标题栏
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnBinder!=null){
            mUnBinder.unbind();
        }
        mActivityUtil.removeActivity(this);
        unregisterReceivers();
    }

//    public void showTipDialog() {
//        tipDialog = new QMUITipDialog.Builder(mContext)
//                .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
//                .setTipWord("正在请求服务器，请稍后...")
//                .create();
//        tipDialog.show();
//    }


//    public void hideTipDialog() {
//        tipDialog.dismiss();
//    }



    /**
     * 接收退出APP的广播消息
     */
    private BroadcastReceiver quitReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            final CustomDialogSureCancel customDialogSureCancel = new CustomDialogSureCancel(BaseActivity.this);
            customDialogSureCancel.getTitleView().setVisibility(View.GONE);
            customDialogSureCancel.getContentView().setText(R.string.single_log_in);
            customDialogSureCancel.getSureView().setVisibility(View.GONE);
            customDialogSureCancel.getCancelView().setText(R.string.confirm);
            customDialogSureCancel.getCancelView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialogSureCancel.cancel();
                    if (null != quitToLoginListener) {
                        quitToLoginListener.onQuitToLogin(BaseActivity.this, 0);
                        quitToLoginListener = null;

                    }

                }
            });
            customDialogSureCancel.show();


        }
    };



    /**
     * 注册广播receiver：由于单点登录的原因，退出APP
     */
    private void registerReceivers() {
        LocalBroadcastManager broadcastMgr = LocalBroadcastManager
                .getInstance(getApplicationContext());
        broadcastMgr.registerReceiver(quitReceiver, new IntentFilter(
                "quit_single_sign_on"));
    }

    /**
     * 取消注册广播receiver：由于单点登录的原因，退出APP
     */
    private void unregisterReceivers() {
        LocalBroadcastManager broadcastMgr = LocalBroadcastManager
                .getInstance(getApplicationContext());
        broadcastMgr.unregisterReceiver(quitReceiver);
    }

    public static void addQuitToLoginListener(IQuitToLogin aQuitToLoginListener) {
        quitToLoginListener = aQuitToLoginListener;
    }

    public static void removeQuitToLoginListern() {
        quitToLoginListener = null;
    }





}

