package com.wlbx.agent.base;

public interface BaseView {
    
    /**
     * 初始化数据
     */
    void initData();
    
    /**
     * 初始化事件
     */
    void initEvent();
}
