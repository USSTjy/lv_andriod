package com.wlbx.agent.net;

import com.wlbx.agent.base.BaseActivity;


public interface IQuitToLogin {
    void onQuitToLogin(BaseActivity fromActivity, int reason);
}
