package com.wlbx.agent.net;

import android.widget.Toast;

import com.wlbx.agent.util.SPUtil;

import java.io.IOException;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public abstract class RetrofitCallBack implements Callback<String> {

    @Override
    public void onResponse(Call<String> call, Response<String> response) {
        if (response.code() == 200) {
            saveHeaders(response.headers());
            onSuccess(response.body());
        } else {
            try {
                onError(new Throwable(response.errorBody().string()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveHeaders(Headers header){
        if("".equals(SPUtil.get("Access-Token","")) && header.get("Access-Token")!=null){
            SPUtil.put("Access-Token",header.get("Access-Token"));
        }
        if(header!=null && "".equals(SPUtil.get("Public-Key","")) && header.get("Access-Token")!=null){
            SPUtil.put("Public-Key",header.get("Public-Key"));
        }
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        onError(t);
    }

    public abstract void onSuccess(String response);

    public abstract void onError(Throwable t);

}
