package com.wlbx.agent.net;


import java.util.List;
import java.util.Map;

import javax.xml.transform.Result;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;


public interface RetrofitRequestInterface {

    /**
     * 密码登录
     */
    @Headers({"Content-Type:application/json"})
    @POST("provider/ims/ejbb/password/login")
    Call<String> loginByPw(@Body String params);

    /**
     * 获取验证码
     */
    @GET("provider/ims/ejbb/sms/validatecode")
    Call<String> getValidCode(@Query("mobile") String mobile);

    /**
     * 手机号验证码登录
     */
    @Headers({"Content-Type:application/json"})
    @POST("provider/ims/ejbb/sms/validatecode/login")
    Call<String> loginByPhone(@Body String params);

    /**
     * 获取代理人详情
     */
    @GET("provider/ims/ejbb/personal/member")
    Call<String> getAgentDetail();

    /**
     * 榜单头条
     */
    @GET("provider/ims/ejbb/firstpage/month/top5")
    Call<String> getBillBoard();

    /**
     * 热销产品列表
     */
    @GET("provider/ims/ejbb/firstpage/product/list")
    Call<String> getHotProducts();

    /**
     * 获取旧手机验证码
     */
    @GET("provider/ims/ejbb/personal/modify/mobile/sms")
    Call<String> getOldValidCode();

    /**
     * 验证旧手机验证码
     */
    @GET("provider/ims/ejbb/personal/check/modify/mobile/sms")
    Call<String> checkOldValidCode(@Query("validateCode") String validateCode);

    /**
     * 获取变更后手机验证码
     */
    @GET("provider/ims/ejbb/personal/modify/latest/mobile/sms")
    Call<String> getNewPhoneValidCode(@Query("mobile") String mobile);

    /**
     * 保存新手机号码
     */
    @Headers({"Content-Type:application/json"})
    @POST("provider/ims/ejbb/personal/modify/mobile")
    Call<String> toSaveNewPhone(@Body String params);

    /**
     * 获取未读消息
     */
    @GET("provider/ims/ejbb/firstpage/message/count/unread")
    Call<String> getUnReadMessage();

    /**
     * 标记消息已读
     */
    @POST("provider/ims/ejbb/message/read/all")
    Call<String> setReaded();

    /**
     * 标记单个消息已读
     */
    @PUT("provider/ims/ejbb/message/read/{id}")
    Call<String> setSingleRead(@Path("id") int id);

    /**
     * 消息列表
     */
    @Headers({"Content-Type:application/json"})
    @POST("provider/ims/ejbb/message/list")
    Call<String> getMessageList(@Body String params);

    /**
     * 设置密码
     */
    @Headers({"Content-Type:application/json"})
    @POST("provider/ims/ejbb/personal/password")
    Call<String> changePw(@Body String params);

    /**
     * 产品列表
     * @param params
     * @return
     */
    @GET("provider/ims/ejbb/product/list")
    Call<String> getProductList(@QueryMap Map<String, Object> params);


    /**
     * 查询出单方式筛选列表
     * @param params
     * @return
     */
    @GET("provider/ims/ejbb/product/inOnline")
    Call<String> getInOnline(@QueryMap Map<String, String> params);


    /**
     * 查询保险机构筛选列表
     * @param params
     * @return
     */
    @GET("provider/ims/ejbb/product/partner")
    Call<String> getPartner(@QueryMap Map<String, String> params);


    /**
     *  查询产品子类型筛选列表
     * @param params
     * @return
     */
    @GET("provider/ims/ejbb/product/subType")
    Call<String> getSubType(@QueryMap Map<String, String> params);

    /**
     * 查询适用人群筛选列表
     * @param params
     * @return
     */
    @GET("provider/ims/ejbb/product/suitableGroup")
    Call<String> getSuitableGroup(@QueryMap Map<String, String> params);


    /**
     * 查询个人资料接口
     *
     */
    @GET("provider/ims/ejbb/personal/member")
    Call<String> getPersonalInfo();



    /**
     * 会员上传头像接口
     */
    @Multipart
    @POST("provider/ims/ejbb/personal/file/upload")
    Call<String> uploadHeader(@Part List<MultipartBody.Part> partList );

    /**
     *查询我的预估收入接口
     *
     */
    @GET("provider/ims/ejbb/personal/income")
    Call<String> getIncomeInfo();

    /**
     * 产品动态列表
     * @param params
     * @return
     */
    @Headers({"Content-Type:application/json"})
    @POST("provider/ims/ejbb/message/product/trend")
    Call<String> productTrendList( @Body String params);


    /**
     * 榜单排名明细
     * @param params
     * @return
     */
    @Headers({"Content-Type:application/json"})
    @POST("provider/ims/ejbb/firstpage/month/top/list")
    Call<String> getBillboardList( @Body String params);


    /**
     * 检查新版本
     * @return
     */
    @GET("provider/ims/ejbb/version/latest/apptype/{appType}/versionno/{versionNo}")
    Call<String> getVersionInfo(@Path("appType") String appType,@Path("versionNo") String versionNo);


    /**
     * 注销账户
     * @param params
     * @return
     */
    @Headers({"Content-Type:application/x-www-form-urlencoded"})
    @POST("provider/ims/ejbb/logout/cancel")
    Call<String> logout( @Body String params);


}

