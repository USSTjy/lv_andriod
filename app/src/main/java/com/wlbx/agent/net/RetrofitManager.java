package com.wlbx.agent.net;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
public class RetrofitManager {
    private Retrofit mRetrofit;
    private OkHttpClient mOkHttpClient;
    private String mBaseUrl;
    private long mConnectTimeout;
    private long mReadTimeout;
    private long mWriteTimeout;
    private static final long DEFAULT_TIMEOUT = 10000;
    private static Context sContext;

    private RetrofitManager(Builder builder) {
        this.mConnectTimeout = builder.mConnectTimeout;
        this.mReadTimeout = builder.mReadTimeout;
        this.mWriteTimeout = builder.mWriteTimeout;
        this.mBaseUrl = builder.mBaseUrl;
        initRetrofit();
    }


    private void initRetrofit() {
        //双向认证
        //证书的inputstream,本地证书的inputstream,本地证书的密码
        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);

        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(mConnectTimeout, TimeUnit.SECONDS)
                .addInterceptor(new TokenInterceptor())


//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Chain chain) throws IOException {
//                        Request request = chain.request()
//                                .newBuilder()
//                                .addHeader("token", SPUtil.getValue(Constant.TOKEN_TAG, Constant.TOKEN))
//                                .build();
//
//                        return chain.proceed(request);
//                    }
//                })
                .writeTimeout(mReadTimeout, TimeUnit.SECONDS)
                .readTimeout(mWriteTimeout, TimeUnit.SECONDS)
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                })
                .sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
                .build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(mOkHttpClient)
                .build();
    }

    public <T> T createRequest(Class<T> requestClass) {
        return mRetrofit.create(requestClass);
    }

    public static final class Builder {
        private String mBaseUrl;
        private long mConnectTimeout;
        private long mReadTimeout;
        private long mWriteTimeout;

        public Builder() {
            mConnectTimeout = DEFAULT_TIMEOUT;
            mReadTimeout = DEFAULT_TIMEOUT;
            mWriteTimeout = DEFAULT_TIMEOUT;
        }

        public Builder baseUrl(String baseUrl) {
            mBaseUrl = baseUrl;
            return this;
        }

        public Builder connectTimeout(long timeout) {
            mConnectTimeout = timeout;
            return this;
        }

        public Builder readTimeout(long timeout) {
            mReadTimeout = timeout;
            return this;
        }

        public Builder writeTimeout(long timeout) {
            mWriteTimeout = timeout;
            return this;
        }

        public RetrofitManager build() {
            return new RetrofitManager(this);
        }
    }



}
