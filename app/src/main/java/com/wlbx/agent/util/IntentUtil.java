package com.wlbx.agent.util;

import android.app.Activity;
import android.content.Intent;

import com.wlbx.agent.bean.ProductListBean;
import com.wlbx.agent.ui.activity.BillboardActivity;
import com.wlbx.agent.ui.activity.LoginActivity;
import com.wlbx.agent.ui.activity.MainActivity;
import com.wlbx.agent.ui.activity.PersonalActivity;
import com.wlbx.agent.ui.activity.ProductNewsActivity;
import com.wlbx.agent.ui.activity.SearchActivity;
import com.wlbx.agent.ui.activity.SettingActivity;
import com.wlbx.agent.ui.activity.WanLiRecommendActivity;
import com.wlbx.agent.ui.activity.WebActivity;


/**
 * 所有跳转操作的工具类
 */
public class IntentUtil {

    public static void goSearchActivity(Activity from,String flag) {
        Intent intent = new Intent();
        intent.setClass(from, SearchActivity.class);
        intent.putExtra("flag",flag);
        from.startActivity(intent);
    }


    public static void goSettingActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, SettingActivity.class);
        from.startActivity(intent);
    }

    public static void goProductNewsActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, ProductNewsActivity.class);
        from.startActivity(intent);
    }

    public static void goWanLiRecommendActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WanLiRecommendActivity.class);
        from.startActivity(intent);
    }



    public static void goBillboardActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, BillboardActivity.class);
        from.startActivity(intent);
    }

    public static void goHelpCenterActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.helpUrl);
        from.startActivity(intent);
    }

    public static void goJoinWanliActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.joinUrl);
        from.startActivity(intent);
    }

    public static void goCustomerListActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.customersUrl);
        from.startActivity(intent);
    }


    public static void goMyOrderActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.ordersUrl);
        from.startActivity(intent);
    }

    public static void goMyClaimActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.claimsUrl);
        from.startActivity(intent);
    }

    public static void goPersonalActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, PersonalActivity.class);
        from.startActivity(intent);
    }

    public static void goBillboardDetailActivity(String previewOrderImg,Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.topDetail+"?previewOrderImg="+previewOrderImg);
        from.startActivity(intent);
    }

    public static void goProductDetailActivity(String sendTime,String readFlag,int id,int productId,String messageTitle,String messageContent,String productUrl,Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.productDetailUrl+"?sendTime="+sendTime+"&readFlag="+readFlag+"&id="+id+"&productId="+productId+"&messageTitle="+messageTitle+"&messageContent="+messageContent+"&productUrl="+productUrl);
        from.startActivity(intent);
    }

    public static void goWanliOfflineActivity(int productId,String productName,Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.productListOffLineUrl+"?productId="+productId+"&productName="+productName);
        from.startActivity(intent);
    }

    public static void goReportActivity(int productId,Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.reportUrl+"?productId="+productId);
        from.startActivity(intent);
    }

    public static void goRevenueActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.revenueUrl);
        from.startActivity(intent);
    }


    public static void goOfflineCarActivity(int productId,String productName,String isCarFlag,Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("purl", Constants.offlineCarUrl+"?productId="+productId+"&productName="+productName+"&isCarFlag="+isCarFlag);
        from.startActivity(intent);
    }


    public static void goLoginActivity(Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, LoginActivity.class);
        from.startActivity(intent);
    }

    public static void goProductDetailActivity(ProductListBean.DataDTO.ListDTO bean,Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, WebActivity.class);
        intent.putExtra("bean",bean);
        from.startActivity(intent);
    }

    public static void goMainActivity(String to,Activity from) {
        Intent intent = new Intent();
        intent.setClass(from, MainActivity.class);
        intent.putExtra("to", to);
//        intent.putExtra("keyword", keyword);
//        from.setResult(Activity.RESULT_OK,intent);
        from.startActivity(intent);
//        from.finish();
    }





}

