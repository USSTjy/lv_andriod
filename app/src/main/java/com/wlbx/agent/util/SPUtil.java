package com.wlbx.agent.util;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.core.content.SharedPreferencesCompat;
import android.util.Base64;

import com.wlbx.agent.global.App;
import com.wlbx.agent.global.Constant;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Created by licrynoob on 2016/guide_2/12 <br>
 * Copyright (C) 2016 <br>
 * Email:licrynoob@gmail.com <p>
 * SharedPreferences封装类
 */
public class SPUtil {

    private static SharedPreferences sp;
    private static Context context;

    static {
        context = App.getInstance();
    }

    private static void init(final String tag) {
        sp = context.getSharedPreferences(tag, Context.MODE_PRIVATE);
    }

    public static String getValue(final String tag, final String key) {
        init(tag);
        return sp.getString(key, "");
    }

    public static int getInt(final String tag, final String key) {
        init(tag);
        return sp.getInt(key, 0);
    }

    public static boolean getBoolean(final String tag, final String key) {
        init(tag);
        return sp.getBoolean(key, false);
    }

    public static void putValue(final String tag, final String key, final String value) {
        init(tag);
        sp.edit().putString(key, value).commit();
    }

    public static void putValue(final String tag, final String key, final int value) {
        init(tag);
        sp.edit().putInt(key, value).commit();
    }

    public static void putValue(final String tag, final String key, final boolean value) {
        init(tag);
        sp.edit().putBoolean(key, value).commit();
    }

    public static void removeValue(final String tag, final String key) {
        init(tag);
        if (sp.contains(key)) {
            sp.edit().remove(key).commit();
        }
    }

    public static void putSet(final String tag, final String key, final Set<String> value) {
        init(tag);
        sp.edit().putStringSet(key, value).commit();
    }


    public static Set<String> getSet(final String tag, final String key) {
        init(tag);
        return sp.getStringSet(key, null);

    }


    public static void putBean(final String tag, String key, Object obj) {
        init(tag);
        if (obj instanceof Serializable) {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(obj);
                String string64 = new String(Base64.encode(baos.toByteArray(),
                        0));
                sp.edit().putString(key, string64).commit();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            throw new IllegalArgumentException(
                    "the obj must implement Serializable");
        }

    }

    public static Object getBean(final String tag, String key) {
        init(tag);
        Object obj = null;
        try {
            String base64 = sp.getString(key, "");
            if (base64.equals("")) {
                return null;
            }
            byte[] base64Bytes = Base64.decode(base64.getBytes(), 1);
            ByteArrayInputStream bais = new ByteArrayInputStream(base64Bytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            obj = ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * 存入数据
     *
     * @param key   键
     * @param value 值
     */
    public static void put(String key, Object value) {
        SharedPreferences.Editor editor = getSharePreferences().edit();
        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else {
            editor.putString(key, value.toString());
        }
        SharedPreferencesCompat.EditorCompat.getInstance().apply(editor);
    }

    /**
     * 获取数据
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return Object value
     */
    public static Object get(String key, Object defaultValue) {
        if (defaultValue instanceof String) {
            return getSharePreferences().getString(key, (String) defaultValue);
        } else if (defaultValue instanceof Integer) {
            return getSharePreferences().getInt(key, (Integer) defaultValue);
        } else if (defaultValue instanceof Boolean) {
            return getSharePreferences().getBoolean(key, (Boolean) defaultValue);
        } else if (defaultValue instanceof Float) {
            return getSharePreferences().getFloat(key, (Float) defaultValue);
        } else if (defaultValue instanceof Long) {
            return getSharePreferences().getLong(key, (Long) defaultValue);
        }
        return null;
    }

    /**
     * 移除某个key值已经对应的值
     *
     * @param key 键
     */
    public static void remove(String key) {
        SharedPreferences.Editor editor = getSharePreferences().edit();
        editor.remove(key);
        SharedPreferencesCompat.EditorCompat.getInstance().apply(editor);
    }

    /**
     * 清除所有数据
     */
    public static void clearAll() {
        SharedPreferences.Editor editor = getSharePreferences().edit();
        editor.clear();
        SharedPreferencesCompat.EditorCompat.getInstance().apply(editor);
    }

    /**
     * 查询某个key是否已经存在
     *
     * @param key 键
     * @return true 存在
     */
    public static boolean contains(String key) {
        return getSharePreferences().contains(key);
    }

    /**
     * 返回所有的键值对
     *
     * @return All key & value
     */
    public static Map<String, ?> getAll() {
        return getSharePreferences().getAll();
    }

    /**
     * 获取 SharedPreferences
     *
     * @return SharedPreferences
     */
    public static SharedPreferences getSharePreferences() {
        return App.getInstance().getSharedPreferences(Constant.SP_NAME, Context.MODE_PRIVATE);
    }

}
