package com.wlbx.agent.util.fingerprint;

import android.os.Handler;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import com.wlbx.agent.ui.activity.MainActivity;

public class MyAuthCallback extends FingerprintManagerCompat.AuthenticationCallback {
	
	
	private Handler handler = null;
	
	public MyAuthCallback(Handler handler) {
        super();

        this.handler = handler;
    }
	

	@Override
	public void onAuthenticationError(int errorCode, CharSequence errString) {
		// TODO Auto-generated method stub
		super.onAuthenticationError(errorCode, errString);
		 if (handler != null) {
	            handler.obtainMessage(MainActivity.MSG_AUTH_ERROR, errorCode, 0).sendToTarget();
	        }
	}
	
	
	@Override
	public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
		// TODO Auto-generated method stub
		super.onAuthenticationHelp(helpCode, helpString);
		 if (handler != null) {
	            handler.obtainMessage(MainActivity.MSG_AUTH_HELP, helpCode, 0).sendToTarget();
	        }
	}
	
	
	
	@Override
	public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
		// TODO Auto-generated method stub
		super.onAuthenticationSucceeded(result);
		 if (handler != null) {
	            handler.obtainMessage(MainActivity.MSG_AUTH_SUCCESS).sendToTarget();
	        }
	}
	
	
	
	@Override
	public void onAuthenticationFailed() {
		// TODO Auto-generated method stub
		super.onAuthenticationFailed();
		 if (handler != null) {
	            handler.obtainMessage(MainActivity.MSG_AUTH_FAILED).sendToTarget();
	        }
	}
	
	
	
	
	
	
	
	
}
