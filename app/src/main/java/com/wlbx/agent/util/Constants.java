package com.wlbx.agent.util;

import com.wlbx.agent.BuildConfig;

public class Constants {
    public static final int PERMISSION_STORAGE = 1000;
    public static final int PERMISSION_CAMRA = 2000;
    public static final int REQUEST_ALBUM = 100;
    public static final int REQUEST_PICKER_AND_CROP = 200;
    public static final int REQUEST_TAKE_PHOTO = 300;
    public static final int REQUEST_CROP = 400;
    public static final int LIMIT_FILE_SIZE = 1024 * 1024 * 10;//限制文件大小为20M


    public static final String H5_BASE_URL= BuildConfig.H5_BASE_URL;

    //录单  active=N,投保 active=Y url
    public static final String orderInputUrl=H5_BASE_URL+"product";
    //首页搜索url
    public static final String searchUrl=H5_BASE_URL+"searchHistory";
    //平台消息 id=10000130&sendData=2021-09-30&sendTime =11:32:31
    public static final String sysMesUrl=H5_BASE_URL+"msgDetail";
    //我的消息
    public static final String mineMesUrl=H5_BASE_URL+"";
    //榜单详情  previewOrderImg=https%3A%2F%2Fceshi.wanlibaoxian.com%3A444%2FinsuredImg%2F2021%5C09%5C22%5C10010836_369000000.jpg
    public static final String topDetail=H5_BASE_URL+"recordDetail";
    //直播回放
    public static final String onLiveUrl=H5_BASE_URL+"review";
    //个人资料展示页	点击“个人头像”进入H5页面
    public static final String personalUrl=H5_BASE_URL+"myInfo";
    // 全部订单展示页	点击“我的订单”进入H5页面
    public static final String ordersUrl=H5_BASE_URL+"myOrder";
    // 全部订单展示页	点击“我的理赔”进入H5页面
    public static final String claimsUrl=H5_BASE_URL+"claims";
    // 全部客户页面   点击“客户管理”进入H5页面
    public static final String customersUrl=H5_BASE_URL+"customer";
    // 帮助中心展示页	点击“帮助中心”进入H5页面
    public static final String helpUrl=H5_BASE_URL+"help";
    // 加入万利页	 点击“加入万利”进入H5页面
    public static final String joinUrl=H5_BASE_URL+"joinUs";
    // 首页--服务协议
    public static final String protocolUrl=H5_BASE_URL+"loginPrivacy";
    // 首页--隐私
    public static final String privateUrl=H5_BASE_URL+"loginRegister";
    // 产品动态详情	rank=2&productId=1815&sendTime=2021-09-01%2017%3A22%3A40&messageSubType=04&messageTitle=测试是his&messageContent=ceshi%20%0A测试&readFlag=Y&messagePicUrl=https%3A%2F%2Fceshi.wanlibaoxian.com%3A444%2FproductImage%2Fputonproduct.jpg&productUrl=https%3A%2F%2Fwww.inswin.cn%2Fr101273%2Fjiankang-baoxian%2F329375.shtml%3FuserTrack%3D%40%40%402092		点击“产品动态”列表中的产品进入H5页面，传递所有的值
    public static final String productDetailUrl=H5_BASE_URL+"productDynamicsDetail";
    //万利优选/产品列表---线上	productId=1867&productUrl=  点击“产品"列表进入H5页面
    public static final String productListOnLineUrl=H5_BASE_URL+"productOnline";
    //万利优选/产品列表---线下	productId=622&productName=平安传福一生二号终身寿险   点击“产品"列表进入H5页面
    public static final String productListOffLineUrl=H5_BASE_URL+"productOffline";
    //累计计件
    public static final String revenueUrl=H5_BASE_URL+"revenue";
    //万利优选/产品列表---线下-车险	productId=1867&productUrl= 可以根据产品列表中的isCarFlag字段做区分
    public static final String offlineCarUrl=H5_BASE_URL+"car";

    //微信分享
    public static final String WX_APPID = "wx7f9b96af50978b1d";

    public static final String reportUrl=H5_BASE_URL+"customerNotification";




    /******************** 存储相关常量 ********************/
    /**
     * Byte与Byte的倍数
     */
    public static final int BYTE = 1;
    /**
     * KB与Byte的倍数
     */
    public static final int KB = 1024;
    /**
     * MB与Byte的倍数
     */
    public static final int MB = 1048576;
    /**
     * GB与Byte的倍数
     */
    public static final int GB = 1073741824;
    /**
     * 毫秒与毫秒的倍数
     */
    public static final int MSEC = 1;

    /******************** 时间相关常量 ********************/
    /**
     * 秒与毫秒的倍数
     */
    public static final int SEC = 1000;
    /**
     * 分与毫秒的倍数
     */
    public static final int MIN = 60000;
    /**
     * 时与毫秒的倍数
     */
    public static final int HOUR = 3600000;
    /**
     * 天与毫秒的倍数
     */
    public static final int DAY = 86400000;
    /**
     * 正则：手机号（简单）
     */
    public static final String REGEX_MOBILE_SIMPLE = "^[1]\\d{10}$";
    /**
     * 正则：手机号（精确）
     * 移动：134(0-8)、135、136、137、138、139、147、150、151、152、157、158、159、178、182、183、184、187、188、199
     * 联通：130、131、132、145、152、155、156、175、176、185、186
     * 电信：133、153、173、177、180、181、189
     * 全球星卫通：1349
     * 虚拟运营商：170
     */
    public static final String REGEX_MOBILE_EXACT = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(16[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|(147)|(19[0-9]))\\d{8}$";

    /******************** 正则相关常量 ********************/

    //--------------------------------------------正则表达式-----------------------------------------
    /**
     * 原文链接：http://caibaojian.com/regexp-example.html
     * 提取信息中的网络链接:(h|H)(r|R)(e|E)(f|F) *= *('|")?(\w|\\|\/|\.)+('|"| *|>)?
     * 提取信息中的邮件地址:\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*
     * 提取信息中的图片链接:(s|S)(r|R)(c|C) *= *('|")?(\w|\\|\/|\.)+('|"| *|>)?
     * 提取信息中的IP地址:(\d+)\.(\d+)\.(\d+)\.(\d+)
     * 提取信息中的中国电话号码（包括移动和固定电话）:(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}
     * 提取信息中的中国邮政编码:[1-9]{1}(\d+){5}
     * 提取信息中的中国身份证号码:\d{18}|\d{15}
     * 提取信息中的整数：\d+
     * 提取信息中的浮点数（即小数）：(-?\d*)\.?\d+
     * 提取信息中的任何数字 ：(-?\d*)(\.\d+)?
     * 提取信息中的中文字符串：[\u4e00-\u9fa5]*
     * 提取信息中的双字节字符串 (汉字)：[^\x00-\xff]*
     */
    /**
     * 正则：电话号码
     */
    public static final String REGEX_TEL = "^0\\d{2,3}[- ]?\\d{7,8}";
    /**
     * 正则：身份证号码15位
     */
    public static final String REGEX_IDCARD15 = "^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$";
    /**
     * 正则：身份证号码18位
     */
    public static final String REGEX_IDCARD18 = "^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9Xx])$";
    /**
     * 正则：身份证号码15或18位 包含以x结尾
     */
    public static final String REGEX_IDCARD = "(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$|^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|x|X)$)";
    /**
     * 正则：邮箱
     */
    public static final String REGEX_EMAIL = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
    /**
     * 正则：URL
     */
    public static final String REGEX_URL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?";
    /**
     * 正则：汉字
     */
    public static final String REGEX_CHZ = "^[\\u4e00-\\u9fa5]+$";
    /**
     * 正则：用户名，取值范围为a-z,A-Z,0-9,"_",汉字，不能以"_"结尾,用户名必须是6-20位
     */
    public static final String REGEX_USERNAME = "^[\\w\\u4e00-\\u9fa5]{6,20}(?<!_)$";
    /**
     * 正则：yyyy-MM-dd格式的日期校验，已考虑平闰年
     */
    public static final String REGEX_DATE = "^(?:(?!0000)[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)-02-29)$";
    /**
     * 正则：IP地址
     */
    public static final String REGEX_IP = "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";

    public enum MemoryUnit {
        BYTE,
        KB,
        MB,
        GB
    }

    public enum TimeUnit {
        MSEC,
        SEC,
        MIN,
        HOUR,
        DAY
    }




}
